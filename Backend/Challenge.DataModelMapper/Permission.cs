﻿using System;
using System.Collections.Generic;

namespace Challenge.DataModelMapper
{
    public partial class Permission
    {
        public long IdRole { get; set; }
        public long IdResource { get; set; }
        public long IdAction { get; set; }

        public virtual Action IdActionNavigation { get; set; }
        public virtual Resource IdResourceNavigation { get; set; }
        public virtual Role IdRoleNavigation { get; set; }
    }
}

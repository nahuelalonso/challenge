﻿using System;
using System.Collections.Generic;

namespace Challenge.DataModelMapper
{
    public partial class UserOfSystem
    {
        public UserOfSystem()
        {
            UserLogin = new HashSet<UserLogin>();
        }

        public long Id { get; set; }
        public long IdEmployee { get; set; }
        public long IdRole { get; set; }
        public DateTime Created { get; set; }
        public string Username { get; set; }
        public string PasswordEncrypted { get; set; }

        public virtual Employee IdEmployeeNavigation { get; set; }
        public virtual Role IdRoleNavigation { get; set; }
        public virtual ICollection<UserLogin> UserLogin { get; set; }
    }
}

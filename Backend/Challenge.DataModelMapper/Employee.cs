﻿using System;
using System.Collections.Generic;

namespace Challenge.DataModelMapper
{
    public partial class Employee
    {
        public Employee()
        {
            EmployeePhoto = new HashSet<EmployeePhoto>();
            UserOfSystem = new HashSet<UserOfSystem>();
            VoteIdEmployeeVotedNavigation = new HashSet<Vote>();
            VoteIdEmployeeVoterNavigation = new HashSet<Vote>();
        }

        public long Id { get; set; }
        public long IdCompany { get; set; }
        public DateTime Created { get; set; }
        public string Docket { get; set; }
        public string Name { get; set; }

        public virtual Company IdCompanyNavigation { get; set; }
        public virtual ICollection<EmployeePhoto> EmployeePhoto { get; set; }
        public virtual ICollection<UserOfSystem> UserOfSystem { get; set; }
        public virtual ICollection<Vote> VoteIdEmployeeVotedNavigation { get; set; }
        public virtual ICollection<Vote> VoteIdEmployeeVoterNavigation { get; set; }
    }
}

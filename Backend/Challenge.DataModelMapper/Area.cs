﻿using System;
using System.Collections.Generic;

namespace Challenge.DataModelMapper
{
    public partial class Area
    {
        public Area()
        {
            CompanyArea = new HashSet<CompanyArea>();
        }

        public long Id { get; set; }
        public DateTime Created { get; set; }
        public string Name { get; set; }

        public virtual ICollection<CompanyArea> CompanyArea { get; set; }
    }
}

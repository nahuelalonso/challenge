﻿using System;
using System.Collections.Generic;

namespace Challenge.DataModelMapper
{
    public partial class CompanyArea
    {
        public CompanyArea()
        {
            Vote = new HashSet<Vote>();
        }

        public long Id { get; set; }
        public long IdCompany { get; set; }
        public long IdArea { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime? ValidThru { get; set; }

        public virtual Area IdAreaNavigation { get; set; }
        public virtual Company IdCompanyNavigation { get; set; }
        public virtual ICollection<Vote> Vote { get; set; }
    }
}

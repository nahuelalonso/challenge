﻿using System;
using System.Collections.Generic;

namespace Challenge.DataModelMapper
{
    public partial class Photo
    {
        public Photo()
        {
            EmployeePhoto = new HashSet<EmployeePhoto>();
        }

        public long Id { get; set; }
        public string Base64 { get; set; }

        public virtual ICollection<EmployeePhoto> EmployeePhoto { get; set; }
    }
}

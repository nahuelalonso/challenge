﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.DataModelMapper
{
    public class RankingByArea
    {
        public long IdArea { get; set; }
        public string AreaName { get; set; }
        public List<EmployeeRanking> EmployeeList { get; set; }
    }

    public class EmployeeRanking
    {
        public long IdEmployee { get; set; }
        public string EmployeeDocket { get; set; }
        public string EmployeeName { get; set; }
        public int TotalVotes { get; set; }
    }
}

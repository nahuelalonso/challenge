﻿using System;
using System.Collections.Generic;

namespace Challenge.DataModelMapper
{
    public partial class Resource
    {
        public Resource()
        {
            Permission = new HashSet<Permission>();
        }

        public long Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Permission> Permission { get; set; }
    }
}

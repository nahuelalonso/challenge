﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Challenge.DataModelMapper
{
    public partial class ChallengeContext : DbContext
    {
        public ChallengeContext()
        {
        }

        public ChallengeContext(DbContextOptions<ChallengeContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Area> Area { get; set; }
        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<CompanyArea> CompanyArea { get; set; }
        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<EmployeePhoto> EmployeePhoto { get; set; }
        public virtual DbSet<Photo> Photo { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<UserLogin> UserLogin { get; set; }
        public virtual DbSet<UserOfSystem> UserOfSystem { get; set; }
        public virtual DbSet<Vote> Vote { get; set; }
        public virtual DbSet<VoteComment> VoteComment { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Area>(entity =>
            {
                entity.ToTable("area");

                entity.HasIndex(e => e.Name)
                    .HasName("area_name")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.Created).HasColumnName("created");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(320);
            });

            modelBuilder.Entity<Company>(entity =>
            {
                entity.ToTable("company");

                entity.HasIndex(e => e.Name)
                    .HasName("name")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.Created).HasColumnName("created");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(320);
            });

            modelBuilder.Entity<CompanyArea>(entity =>
            {
                entity.ToTable("company_area");

                entity.HasIndex(e => e.IdArea)
                    .HasName("ix_pfk_company_area_area");

                entity.HasIndex(e => e.IdCompany)
                    .HasName("ix_pfk_company_area_company");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.IdArea).HasColumnName("id_area");

                entity.Property(e => e.IdCompany).HasColumnName("id_company");

                entity.Property(e => e.ValidFrom).HasColumnName("valid_from");

                entity.Property(e => e.ValidThru).HasColumnName("valid_thru");

                entity.HasOne(d => d.IdAreaNavigation)
                    .WithMany(p => p.CompanyArea)
                    .HasForeignKey(d => d.IdArea)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("pfk_company_area_area");

                entity.HasOne(d => d.IdCompanyNavigation)
                    .WithMany(p => p.CompanyArea)
                    .HasForeignKey(d => d.IdCompany)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("pfk_company_area_company");
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.ToTable("employee");

                entity.HasIndex(e => e.IdCompany)
                    .HasName("idx_fk_employee_id_company");

                entity.HasIndex(e => new { e.IdCompany, e.Docket })
                    .HasName("idx_id_company_docket")
                    .IsUnique();

                entity.HasIndex(e => new { e.IdCompany, e.Name })
                    .HasName("idx_id_company_name");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.Created).HasColumnName("created");

                entity.Property(e => e.Docket)
                    .IsRequired()
                    .HasColumnName("docket")
                    .HasMaxLength(40);

                entity.Property(e => e.IdCompany).HasColumnName("id_company");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(240);

                entity.HasOne(d => d.IdCompanyNavigation)
                    .WithMany(p => p.Employee)
                    .HasForeignKey(d => d.IdCompany)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_employee_company");
            });

            modelBuilder.Entity<EmployeePhoto>(entity =>
            {
                entity.ToTable("employee_photo");

                entity.HasIndex(e => e.IdEmployee)
                    .HasName("idx_fk_employee_photo_id_employee");

                entity.HasIndex(e => e.IdPhoto)
                    .HasName("idx_fk_employee_photo_id_photo");

                entity.HasIndex(e => new { e.IdEmployee, e.Active })
                    .HasName("idx_employee_active");

                entity.HasIndex(e => new { e.IdEmployee, e.UploadDate })
                    .HasName("idx_employee_upload_date");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.Active).HasColumnName("active");

                entity.Property(e => e.IdEmployee).HasColumnName("id_employee");

                entity.Property(e => e.IdPhoto).HasColumnName("id_photo");

                entity.Property(e => e.UploadDate).HasColumnName("upload_date");

                entity.HasOne(d => d.IdEmployeeNavigation)
                    .WithMany(p => p.EmployeePhoto)
                    .HasForeignKey(d => d.IdEmployee)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_employee_photo_employee");

                entity.HasOne(d => d.IdPhotoNavigation)
                    .WithMany(p => p.EmployeePhoto)
                    .HasForeignKey(d => d.IdPhoto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_employee_photo_photo");
            });

            modelBuilder.Entity<Photo>(entity =>
            {
                entity.ToTable("photo");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.Base64)
                    .IsRequired()
                    .HasColumnName("base64");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("role");

                entity.HasIndex(e => e.Name)
                    .HasName("role_name")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.Created).HasColumnName("created");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(120);
            });

            modelBuilder.Entity<UserLogin>(entity =>
            {
                entity.ToTable("user_login");

                entity.HasIndex(e => e.IdUser)
                    .HasName("idx_fk_user_login_id_user");

                entity.HasIndex(e => e.LoginDate)
                    .HasName("idx_login_date");

                entity.HasIndex(e => new { e.IdUser, e.LoginDate })
                    .HasName("idx_id_user_login_date");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.FromIp)
                    .IsRequired()
                    .HasColumnName("from_ip")
                    .HasMaxLength(39);

                entity.Property(e => e.IdUser).HasColumnName("id_user");

                entity.Property(e => e.LoginDate).HasColumnName("login_date");

                entity.HasOne(d => d.IdUserNavigation)
                    .WithMany(p => p.UserLogin)
                    .HasForeignKey(d => d.IdUser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_user_login_user");
            });

            modelBuilder.Entity<UserOfSystem>(entity =>
            {
                entity.ToTable("user_of_system");

                entity.HasIndex(e => e.IdEmployee)
                    .HasName("idx_fk_user_id_employee");

                entity.HasIndex(e => e.IdRole)
                    .HasName("idx_fk_user_id_role");

                entity.HasIndex(e => e.Username)
                    .HasName("username")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.Created).HasColumnName("created");

                entity.Property(e => e.IdEmployee).HasColumnName("id_employee");

                entity.Property(e => e.IdRole).HasColumnName("id_role");

                entity.Property(e => e.PasswordEncrypted)
                    .IsRequired()
                    .HasColumnName("password_encrypted")
                    .HasMaxLength(500);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username")
                    .HasMaxLength(120);

                entity.HasOne(d => d.IdEmployeeNavigation)
                    .WithMany(p => p.UserOfSystem)
                    .HasForeignKey(d => d.IdEmployee)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_user_employee");

                entity.HasOne(d => d.IdRoleNavigation)
                    .WithMany(p => p.UserOfSystem)
                    .HasForeignKey(d => d.IdRole)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_user_role");
            });

            modelBuilder.Entity<Vote>(entity =>
            {
                entity.ToTable("vote");

                entity.HasIndex(e => e.IdCompanyArea)
                    .HasName("idx_fk_vote_company_area");

                entity.HasIndex(e => e.IdEmployeeVoted)
                    .HasName("idx_fk_vote_employee_voted");

                entity.HasIndex(e => e.IdEmployeeVoter)
                    .HasName("idx_fk_vote_employee_voter");

                entity.HasIndex(e => new { e.IdEmployeeVoted, e.Date })
                    .HasName("idx_employee_voted_area_date");

                entity.HasIndex(e => new { e.IdEmployeeVoter, e.Date })
                    .HasName("idx_employee_voter_area_date");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.Date).HasColumnName("date");

                entity.Property(e => e.IdCompanyArea).HasColumnName("id_company_area");

                entity.Property(e => e.IdEmployeeVoted).HasColumnName("id_employee_voted");

                entity.Property(e => e.IdEmployeeVoter).HasColumnName("id_employee_voter");

                entity.HasOne(d => d.IdCompanyAreaNavigation)
                    .WithMany(p => p.Vote)
                    .HasForeignKey(d => d.IdCompanyArea)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_vote_company_area");

                entity.HasOne(d => d.IdEmployeeVotedNavigation)
                    .WithMany(p => p.VoteIdEmployeeVotedNavigation)
                    .HasForeignKey(d => d.IdEmployeeVoted)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_vote_employee_voted");

                entity.HasOne(d => d.IdEmployeeVoterNavigation)
                    .WithMany(p => p.VoteIdEmployeeVoterNavigation)
                    .HasForeignKey(d => d.IdEmployeeVoter)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_vote_employee_voter");
            });

            modelBuilder.Entity<VoteComment>(entity =>
            {
                entity.HasKey(e => e.IdVote)
                    .HasName("pk_vote_comment");

                entity.ToTable("vote_comment");

                entity.Property(e => e.IdVote)
                    .HasColumnName("id_vote")
                    .ValueGeneratedNever();

                entity.Property(e => e.Comment)
                    .IsRequired()
                    .HasColumnName("comment");

                entity.HasOne(d => d.IdVoteNavigation)
                    .WithOne(p => p.VoteComment)
                    .HasForeignKey<VoteComment>(d => d.IdVote)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("pfk_vote_comment");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

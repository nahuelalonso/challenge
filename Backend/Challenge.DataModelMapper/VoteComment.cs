﻿using System;
using System.Collections.Generic;

namespace Challenge.DataModelMapper
{
    public partial class VoteComment
    {
        public long IdVote { get; set; }
        public string Comment { get; set; }

        public virtual Vote IdVoteNavigation { get; set; }
    }
}

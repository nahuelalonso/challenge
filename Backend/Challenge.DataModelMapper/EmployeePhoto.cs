﻿using System;
using System.Collections.Generic;

namespace Challenge.DataModelMapper
{
    public partial class EmployeePhoto
    {
        public long Id { get; set; }
        public long IdEmployee { get; set; }
        public long IdPhoto { get; set; }
        public DateTime UploadDate { get; set; }
        public bool Active { get; set; }

        public virtual Employee IdEmployeeNavigation { get; set; }
        public virtual Photo IdPhotoNavigation { get; set; }
    }
}

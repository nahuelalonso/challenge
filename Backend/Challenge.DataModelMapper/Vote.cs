﻿using System;
using System.Collections.Generic;

namespace Challenge.DataModelMapper
{
    public partial class Vote
    {
        public long Id { get; set; }
        public long IdEmployeeVoter { get; set; }
        public long IdEmployeeVoted { get; set; }
        public long IdCompanyArea { get; set; }
        public DateTime Date { get; set; }

        public virtual CompanyArea IdCompanyAreaNavigation { get; set; }
        public virtual Employee IdEmployeeVotedNavigation { get; set; }
        public virtual Employee IdEmployeeVoterNavigation { get; set; }
        public virtual VoteComment VoteComment { get; set; }
    }
}

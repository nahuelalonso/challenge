﻿using System;
using System.Collections.Generic;

namespace Challenge.DataModelMapper
{
    public partial class Role
    {
        public Role()
        {
            UserOfSystem = new HashSet<UserOfSystem>();
        }

        public long Id { get; set; }
        public DateTime Created { get; set; }
        public string Name { get; set; }

        public virtual ICollection<UserOfSystem> UserOfSystem { get; set; }
    }
}

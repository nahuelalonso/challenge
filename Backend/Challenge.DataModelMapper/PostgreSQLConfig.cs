﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.DataModelMapper
{
    public class PostgreSQLConfig
    {
        public string Host { get; set; } = "localhost";
        public int Port { get; set; } = 5432;
        public string Username { get; set; } = "postgres";
        public string Password { get; set; } = "";
        public string DatabaseName { get; set; } = "challange";
        public int MaxPoolSize { get; set; } = 20;
        public bool Pooling { get; set; } = true;
        public string ConnectionString => $"Host={Host};Port={Port};Database={DatabaseName};Username={Username};Password={Password};Pooling={(Pooling?"true":"false")};MinPoolSize=0;MaxPoolSize={MaxPoolSize}";
    } 
}

﻿using System;
using System.Collections.Generic;

namespace Challenge.DataModelMapper
{
    public partial class UserLogin
    {
        public long Id { get; set; }
        public long IdUser { get; set; }
        public DateTime LoginDate { get; set; }
        public string FromIp { get; set; }

        public virtual UserOfSystem IdUserNavigation { get; set; }
    }
}

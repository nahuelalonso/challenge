﻿using System;
using System.Collections.Generic;

namespace Challenge.DataModelMapper
{
    public partial class Company
    {
        public Company()
        {
            CompanyArea = new HashSet<CompanyArea>();
            Employee = new HashSet<Employee>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }

        public virtual ICollection<CompanyArea> CompanyArea { get; set; }
        public virtual ICollection<Employee> Employee { get; set; }
    }
}

﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Challenge.Helpers
{
    /// <summary>
    /// Ayudante para encrptar y desencriptar data
    /// </summary>
    public static class CryptoHelper
    {
        private const string InternalPassword = "thi$ $4lt k3y i$ m0re difficult t0 d3cry9t";
        private static byte[] EncryptoSalt => new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 };
        private const char CharSplitter = '|';
        private static Random random = new Random(Int32.MaxValue);

        /// <summary>
        /// Función para encriptar texto
        /// </summary>
        /// <param name="clearText">Texto a encriptar</param>
        /// <param name="optionalSalt">Salt adicional en forma opcional</param>
        /// <returns>Cadena de texto encryptada</returns>
        /// <exception cref="DecryptException">Si hay un error al encriptar tira EncryptException</exception>
        public static string Encrypt(string clearText, string optionalSalt = null)
        {
            var randomPass = GetRandom(32);
            if (optionalSalt != null)
                randomPass += optionalSalt;
            string encrypted;
            try
            {
                var clearBytes = Encoding.Unicode.GetBytes(clearText);
                using (var encryptor = Aes.Create())
                {
                    var pdb = new Rfc2898DeriveBytes(InternalPassword + randomPass, EncryptoSalt);
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (var ms = new MemoryStream())
                    {
                        using (var cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        encrypted = Convert.ToBase64String(ms.ToArray());
                    }
                }
            }
            catch (Exception ex)
            {
                throw new EncryptException(ex);
            }
            return $"{encrypted}{CharSplitter}{randomPass}";
        }

        /// <summary>
        /// Función para desencriptar un resultado de la Función Encrypt
        /// </summary>
        /// <param name="cipherText">Cadena de Texto Encriptada</param>
        /// <returns>Texto original</returns>
        /// <exception cref="DecryptException">Si hay un error al desencriptar tira DecryptException</exception>
        public static string Decrypt(string cipherText)
        {
            var toReturn = string.Empty;
            try
            {
                var textSplitted = cipherText.Split(CharSplitter);
                var cipherBytes = Convert.FromBase64String(textSplitted[0]);
                using (var encryptor = Aes.Create())
                {
                    var pdb = new Rfc2898DeriveBytes(InternalPassword + textSplitted[1], EncryptoSalt);
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (var ms = new MemoryStream())
                    {
                        using (var cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }
                        toReturn = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }
            }
            catch (Exception ex)
            {
                throw new DecryptException(ex);
            }
            return toReturn;
        }

        public static string GetRandom(int size = 10)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789$_|-()!?";
            var stringChars = new char[size];
            for (var i = 0; i < size; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }
            return new string(stringChars);
        }
    }

    public class EncryptException : Exception
    {
        public EncryptException(Exception ex) : base("An error ocurred when try encrypt data", ex) { }
    }

    public class DecryptException : Exception
    {
        public DecryptException(Exception ex) : base("An error ocurred when try decrypt data", ex) { }
    }
}

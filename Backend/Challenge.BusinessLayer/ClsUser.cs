﻿using Challenge.DataManager;
using Challenge.DataModelMapper;
using Challenge.Helpers;
using System;
using System.Threading.Tasks;

namespace Challenge.BusinessLayer
{
    /// <summary>
    /// Clase Usuario donde se representa al usuario que quiere interactuar con el sistema
    /// </summary>
    public class ClsUser
    {
        /// <summary>
        /// Id del usuario en la base
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Nombre de usuario
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// Útimo login que hizo el usuario
        /// </summary>
        public ClsLastLogin LastLogin { get; set; }
        /// <summary>
        /// Propiedad del usuario que corresponde a un empleado
        /// </summary>
        public ClsEmployee Employee { get; set; }
        /// <summary>
        /// Propiedad del Role del usuario
        /// </summary>
        public string Role { get; set; }

        private ChallengeRepository Repository { get; set; }

        /// <summary>
        /// Constructor de la Clase Usuario
        /// </summary>
        /// <param name="repository">Repository para poder conectarse con la base</param>
        public ClsUser(ChallengeRepository repository)
        {
            Repository = repository;
        }

        /// <summary>
        /// Función para verificar los datos del usuario en la base de datos
        /// </summary>
        /// <param name="username">Nombre de usuario</param>
        /// <param name="password">Contraseña que viene desde el request</param>
        /// <param name="repository">Repositorio para hacer la consulta a la base</param>
        /// <returns>Devuelve un usuario</returns>
        public async static Task<ClsUser> Login(string username, string password, string remoteIp, ChallengeRepository repository)
        {
            var result = await repository.User.GetByUsername(username);
            if (result == null) throw new NotFoundException("User by username", username);
            try
            {
                var decryptPass = CryptoHelper.Decrypt(result.PasswordEncrypted);
                if (password != decryptPass) throw new UserPasswordException(username);
            } catch (DecryptException)
            {
                throw new UserPasswordException(username);
            }
            var user = new ClsUser(repository);
            await user.ModelToCls(result, remoteIp);
            _ = user.AddLogin();
            return user;
        }

        /// <summary>
        /// Función para crear un usuario
        /// </summary>
        /// <param name="password">Password del usario</param>
        /// <param name="roleName">Nombre del rol con el que se va a crear</param>
        /// <returns></returns>
        public async Task Create(string password, string roleName = "Employee")
        {
            Role = roleName;
            var role = await Repository.Role.GetByName(Role);
            var toAdd = new UserOfSystem
            {
                Created = DateTime.Now,
                IdEmployeeNavigation = new Employee
                {
                    Created = DateTime.Now,
                    Docket = Employee.Docket,
                    Name = Employee.Name,
                    IdCompany = Employee.Company.Id
                },
                IdRole = role.Id,
                PasswordEncrypted = CryptoHelper.Encrypt(password),
                Username = Username
            };
            var result = await Repository.User.Add(toAdd);
            Id = result.Id;
            Employee.Id = result.IdEmployee;
        }

        /// <summary>
        /// Función que verifica si el username ya existe en la base de datos
        /// </summary>
        /// <param name="username">Username que se quiere crear</param>
        /// <param name="repository">Repositorio para poder consultar la base</param>
        /// <returns>Devuelve si existe o no el username</returns>
        public static async Task<bool> Exist(string username, ChallengeRepository repository)
        {
            var result = await repository.User.GetByUsername(username);
            return result != null;
        }

        private async Task AddLogin()
        {
            await Repository.User.AddLastLogin(new UserLogin
            {
                FromIp = LastLogin.FromIp,
                IdUser = Id,
                LoginDate = LastLogin.Date
            });
        }

        private async Task ModelToCls(UserOfSystem userModel, string rempoteIp = null)
        {
            Id = userModel.Id;
            Username = userModel.Username;
            Role = userModel.IdRoleNavigation.Name;
            LastLogin = new ClsLastLogin()
            {
                Date = DateTime.Now,
                FromIp = rempoteIp
            };
            Employee = new ClsEmployee(userModel.IdEmployee, Repository);
            await Employee.Instanciate(true);
        }
    }
}

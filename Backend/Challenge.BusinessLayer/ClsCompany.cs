﻿using Challenge.DataManager;
using Challenge.DataModelMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.BusinessLayer
{
    /// <summary>
    /// Clase Company donde representa la compañia en la que trabaja un empleado
    /// </summary>
    public class ClsCompany
    {
        /// <summary>
        /// Id de la compañia en la base
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Nombre de la compañia
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Listado de Areas que contiene la compañia para votar
        /// </summary>
        public List<ClsArea> Areas { get; set; } = new List<ClsArea>();

        private ChallengeRepository Repository { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repository">Repositorio para que pueda acceder a la base</param>
        public ClsCompany(ChallengeRepository repository)
        {
            Repository = repository;
        }

        /// <summary>
        /// Función que devuelve si la empresa existe o no, según su nombre
        /// </summary>
        /// <param name="name">Nombre de la empresa</param>
        /// <param name="repository">Repositorio para que pueda acceder a la base</param>
        /// <returns>True or False</returns>
        public async static Task<bool> Exist(string name, ChallengeRepository repository)
        {
            var exist = await repository.Company.GetByName(name);
            return exist != null;
        }

        /// <summary>
        /// Función para obtener la cantidad de empleados de la empresa
        /// </summary>
        /// <param name="idCompany">Id de la compañia en la que voy a buscar</param>
        /// <param name="repository">Repositorio para que pueda acceder a la base</param>
        /// <returns>Total de empleados que tiene la empresa</returns>
        public static async Task<int> GetCountEmployees(long idCompany, ChallengeRepository repository)
        {
            return await repository.Company.CountEmployee(idCompany);
        }

        /// <summary>
        /// Obtiene un listado de Ranking por mes de los empleados más votados por area
        /// </summary>
        /// <param name="idCompany">Id de la compañia</param>
        /// <param name="dateFrom">Fecha desde</param>
        /// <param name="dateTo">Fecha hasta</param>
        /// <param name="repository">Repositorio para que pueda acceder a la base</param>
        /// <returns>Listado del Ranking del empleado más votado por mes según areas</returns>
        public async static Task<List<RankingByMonth<RankingByArea>>> GetRankingEmployeeByArea(long idCompany, DateTime dateFrom, DateTime dateTo, ChallengeRepository repository)
        {
            var toReturn = new List<RankingByMonth<RankingByArea>>();
            dateFrom = new DateTime(dateFrom.Year, dateFrom.Month, 1);
            dateTo = new DateTime(dateTo.Year, dateTo.Month, 1);
            do
            {
                var result = await repository.Vote.GetByDateRangeForCompanyByArea(idCompany, dateFrom.Month, dateFrom.Year);
                toReturn.Add(new RankingByMonth<RankingByArea>
                {
                    Date = dateFrom,
                    Data = result.Select(current => new RankingByArea
                    {
                        IdArea = current.IdArea,
                        AreaName = current.AreaName,
                        Employees = current.EmployeeList.Select(currentEmployee => new RankingEmployee
                        {
                            IdEmployee = currentEmployee.IdEmployee,
                            EmployeeDocket = currentEmployee.EmployeeDocket,
                            EmployeeName = currentEmployee.EmployeeName,
                            TotalVotes = currentEmployee.TotalVotes
                        }).ToList()
                    }).ToList()
                });
                dateFrom = dateFrom.AddMonths(1);
            } while (dateFrom <= dateTo);
            return toReturn;
        }

        /// <summary>
        /// Obtiene un listado de Ranking por mes de los empleados más votados por mes
        /// </summary>
        /// <param name="idCompany">Id de la compañia</param>
        /// <param name="dateFrom">Fecha desde</param>
        /// <param name="dateTo">Fecha hasta</param>
        /// <param name="repository">Repositorio para que pueda acceder a la base</param>
        /// <returns>Listado del Ranking del empleado más votado por mes</returns>
        public async static Task<List<RankingByMonth<RankingEmployee>>> GetRankingEmployeeByMonth(long idCompany, DateTime dateFrom, DateTime dateTo, ChallengeRepository repository)
        {
            var toReturn = new List<RankingByMonth<RankingEmployee>>();
            dateFrom = new DateTime(dateFrom.Year, dateFrom.Month, 1);
            dateTo = new DateTime(dateTo.Year, dateTo.Month, 1);
            do
            {
                var result = await repository.Vote.GetByDateRangeForCompanyByEmployee(idCompany, dateFrom.Month, dateFrom.Year);
                toReturn.Add(new RankingByMonth<RankingEmployee>
                {
                    Date = dateFrom,
                    Data = result.Select(current => new RankingEmployee {
                        IdEmployee = current.IdEmployee,
                        EmployeeDocket = current.EmployeeDocket,
                        EmployeeName = current.EmployeeName,
                        TotalVotes = current.TotalVotes
                    }).ToList()
                });
                dateFrom = dateFrom.AddMonths(1);
            } while (dateFrom <= dateTo);
            return toReturn;
        }

        /// <summary>
        /// Constructor con Id, para luego poder instanciarlo desde la base de datos
        /// </summary>
        /// <param name="id">Id de la compañia</param>
        /// <param name="repository">Repositorio para que pueda acceder a la base</param>
        public ClsCompany(long id, ChallengeRepository repository)
        {
            Repository = repository;
            Id = id;
        }

        /// <summary>
        /// Función para la creación de la empresa en la base
        /// </summary>
        public async Task Create()
        {
            var company = await Repository.Company.Add(new Company
            {
                Name = Name,
                Created = DateTime.Now
            });
            Id = company.Id;
            foreach (var current in Areas)
            {
                var currentArea = await GetArea(current.Name);
                var companyArea = await Repository.Company.AddCompanyArea(Id, currentArea.Id);
                current.Id = companyArea.Id;
                current.ValidFrom = companyArea.ValidFrom;
                current.ValidThru = companyArea.ValidThru;
            }
        }

        /// <summary>
        /// Función que verifica si la empresa tiene el area deseada
        /// </summary>
        /// <param name="idCompany">Id de la compañia a la que le vamos a preguntar</param>
        /// <param name="idCompanyArea">Id de la relación del area con la compañia</param>
        /// <param name="repository">Repositorio para que pueda acceder a la base</param>
        /// <exception cref="NotFoundException">Cuando la relazión ya expiro del mes corriente</exception>
        public async static Task<bool> HasArea(long idCompany, long idCompanyArea, ChallengeRepository repository)
        {
            var companyArea = await repository.Company.GetCompanyAreas(idCompany);
            if (companyArea == null || companyArea.All(current => current.Id != idCompanyArea))
                return false;
            return true;
        }

        /// <summary>
        /// Función para instanciar la compañia desde la base de datos
        /// </summary>
        /// <param name="loadAreas">Parametro para que cargue las areas de la compañia</param>
        public async Task Instanciate(bool loadAreas = false)
        {
            var company = await Repository.Company.GetById(Id);
            if (company == null) throw new NotFoundException("Company with id", $"{Id}");
            Name = company.Name;
            if (!loadAreas) return;
            var areas = await Repository.Company.GetCompanyAreas(Id);
            foreach(var currentArea in areas)
            {
                var toAdd = new ClsArea()
                {
                    Id = currentArea.Id,
                    Name = currentArea.IdAreaNavigation.Name,
                    ValidFrom = currentArea.ValidFrom,
                    ValidThru = currentArea.ValidThru
                };
                Areas.Add(toAdd);
            }
        }

        /// <summary>
        /// Función para obtener todas las areas que tiene la empresa vigentes en el mes corriente
        /// </summary>
        /// <param name="idCompany">Id de la compañia dueña de las areas</param>
        /// <param name="repository">Repositorio para que pueda acceder a la base</param>
        /// <returns>Lista de ClsArea</returns>
        public async static Task<List<ClsArea>> GetAllAreas(long idCompany, ChallengeRepository repository)
        {
            var companyAreas = await repository.Company.GetCompanyAreas(idCompany);
            return companyAreas.Select(current => new ClsArea
            {
                Id = current.Id,
                Name = current.IdAreaNavigation.Name
            }).ToList();
        }

        private async Task<Area> GetArea(string name)
        {
            return await Repository.Area.Add(new Area
            {
                Name = name,
                Created = DateTime.Now
            });
        }
    }
}

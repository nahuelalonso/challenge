﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.BusinessLayer
{
    public class RankingByMonth<Type>
    {
        public DateTime Date { get; set; }
        public List<Type> Data { get; set; } = new List<Type>();
    }

    public class RankingEmployee
    {
        public long IdEmployee { get; set; }
        public string EmployeeDocket { get; set; }
        public string EmployeeName { get; set; }
        public int TotalVotes { get; set; }
    }

    public class RankingByArea
    {
        public long IdArea { get; set; }
        public string AreaName { get; set; }
        public List<RankingEmployee> Employees { get; set; } = new List<RankingEmployee>();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.BusinessLayer
{
    public class ClsVote
    {
        public long Id { get; set; }
        public DateTime EmitedDate { get; set; }
        public ClsEmployee EmployeeVoted { get; set; }
        public ClsArea AreaVoted { get; set; }
        public String Comment { get; set; }
    }
}

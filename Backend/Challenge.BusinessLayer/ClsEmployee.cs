﻿using Challenge.DataManager;
using Challenge.DataModelMapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Challenge.BusinessLayer
{
    /// <summary>
    /// Clase Empleado donde se representa a una persona que trabaja en una compañia
    /// </summary>
    public class ClsEmployee
    {
        /// <summary>
        /// Id del Empleado en el sistema
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Objeto Compañia a la que pertenece el empleado
        /// </summary>
        public ClsCompany Company { get; set; }
        /// <summary>
        /// Legajo del empleado en la compañia
        /// </summary>
        public string Docket { get; set; }
        /// <summary>
        /// Nombre del Empleado
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Foto de perfil del empleado
        /// </summary>
        public ClsPhoto Photo { get; set; }

        private ChallengeRepository Repository { get; set; }

        /// <summary>
        /// Constructor de la clase Employee
        /// </summary>
        /// <param name="repository">Repositorio para que pueda acceder a la base</param>
        public ClsEmployee( ChallengeRepository repository)
        {
            Repository = repository;
        }

        /// <summary>
        /// Constructor de la clase Employee
        /// </summary>
        /// <param name="id">Id del empleado a buscar en la base</param>
        /// <param name="repository">Repositorio para que pueda acceder a la base</param>
        public ClsEmployee(long id, ChallengeRepository repository)
        {
            Repository = repository;
            Id = id;
        }

        /// <summary>
        /// Función para instanciar desde la base de datos al empleado, con la posibilidad de poder instancias la company a la que pertenece y la foto
        /// </summary>
        /// <param name="loadCompany"></param>
        /// <param name="loadPhoto"></param>
        /// <returns></returns>
        public async Task Instanciate(bool loadCompany = false, bool loadPhoto = false)
        {
            var employee = await Repository.Employee.GetById(Id);
            if (employee == null) throw new NotFoundException("Employee with id", $"{Id}");
            ModelToCls(employee, loadCompany, loadPhoto);
        }

        /// <summary>
        /// Función para determinar si existe ya un empleado con el legajo indicado en una compañia
        /// </summary>
        /// <param name="docket">Legajo del empleado</param>
        /// <param name="idCompany">Id de la compañia en la que trabaja</param>
        /// <param name="repository">Repositorio para que pueda acceder a la base</param>
        /// <returns>Devuelve si existe o no</returns>
        public async static Task<bool> Exist(string docket, long idCompany, ChallengeRepository repository)
        {
            var exist = await repository.Employee.GetByDocketAndIdCompany(docket,idCompany);
            return exist != null;
        }

        /// <summary>
        /// Función para cargar los votos para un mes y año específico para el empleado
        /// </summary>
        /// <param name="month">Mes</param>
        /// <param name="year">Año</param>
        /// <returns>Devuelve la lista de votos para un mes y año concreto</returns>
        public async Task GetVotes(int month, int year)
        {
            var toReturn = new List<ClsVote>();
            var dateFrom = new DateTime(year, month, 1);
            var votes = await Repository.Vote.GetByDateRangeForEmployeeVoter(Id, dateFrom, dateFrom.AddMonths(1));
            foreach (var currentVote in votes)
            {
                var employee = new ClsEmployee(currentVote.IdEmployeeVoted, Repository);
                employee.ModelToCls(currentVote.IdEmployeeVotedNavigation);
                var toAdd = new ClsVote
                {
                    Id = currentVote.Id,
                    EmitedDate = currentVote.Date,
                    Comment = currentVote.VoteComment?.Comment,
                    EmployeeVoted = employee,
                    AreaVoted = new ClsArea
                    {
                        Id = currentVote.IdCompanyArea,
                        Name = currentVote.IdCompanyAreaNavigation.IdAreaNavigation.Name,
                        ValidFrom = currentVote.IdCompanyAreaNavigation.ValidFrom,
                        ValidThru = currentVote.IdCompanyAreaNavigation.ValidThru
                    }
                };
                toReturn.Add(toAdd);
            }
        }

        /// <summary>
        /// Emitir votos
        /// </summary>
        /// <param name="votes">Lista de votos a emitir</param>
        public async Task<List<string>> Vote(List<ClsVote> votes)
        {
            var alreadyVoteInArea = new List<string>();
            foreach (var currentVote in votes)
            {
                try
                {
                    await Vote(currentVote);
                } catch (AlreadyVoteException ex)
                {
                    alreadyVoteInArea.Add(ex.Message);
                }
            }
            return alreadyVoteInArea;
        }

        /// <summary>
        /// Emitir voto
        /// </summary>
        /// <param name="vote">Voto a emitir</param>
        /// <exception cref="AlreadyVoteException">Tira la excepción cuando el voto ya fue emitido en el mes actual</exception>
        public async Task Vote(ClsVote vote)
        {
            var searchFrom = new DateTime(vote.EmitedDate.Year, vote.EmitedDate.Month, 1);
            var alreadyVote = await Repository.Vote.GetByDateRangeAndAreaForEmployeeVoter(Id, vote.AreaVoted.Id, searchFrom, searchFrom.AddMonths(1));
            if (alreadyVote != null) throw new AlreadyVoteException(vote.AreaVoted.Id.ToString());
            var toAdd = new Vote                                                         
            {
                Date = vote.EmitedDate,
                IdEmployeeVoter = Id,
                IdEmployeeVoted = vote.EmployeeVoted.Id,
                IdCompanyArea = vote.AreaVoted.Id
            };
            if (!string.IsNullOrEmpty(vote.Comment))
                toAdd.VoteComment = new VoteComment
                {
                    Comment = vote.Comment
                };
            await Repository.Vote.Add(toAdd);
        }

        /// <summary>
        /// Función para obtener todos los empleados de la compañia en la que trabajo
        /// </summary>
        /// <param name="idCompany">Id de la compañia</param>
        /// <param name="repository">Repositorio para que pueda acceder a la base</param>
        /// <returns>Lista de ClsEmployee</returns>
        public async static Task<List<ClsEmployee>> GetAllByCompany(long idCompany, ChallengeRepository repository)
        {
            var toReturn = new List<ClsEmployee>();
            var employees = await repository.Employee.GetByIdCompany(idCompany);
            foreach (var current in employees)
            {
                toReturn.Add(new ClsEmployee(current.Id, repository)
                {
                    Docket = current.Docket,
                    Name = current.Name,
                    Company = new ClsCompany(idCompany, repository)
                });

            }
            return toReturn;
        }

        private void ModelToCls(Employee employeeModel, bool loadCompany = false, bool loadPhoto = false)
        {

            Docket = employeeModel.Docket;
            Name = employeeModel.Name;
            Company = new ClsCompany(employeeModel.IdCompany, Repository);
            var listToAwait = new List<Task>();
            if (loadCompany)
                listToAwait.Add(Company.Instanciate());
            if (loadPhoto)
                listToAwait.Add(LoadPhoto());
            if (listToAwait.Count == 0) return;
            Task.WaitAll(listToAwait.ToArray());
        }

        private async Task LoadPhoto()
        {
            var photo = await Repository.Employee.GetPhoto(Id);
            if (photo == null) return;
            Photo = new ClsPhoto
            {
                Id = photo.Id,
                UploadDate = photo.UploadDate,
                Base64 = photo.IdPhotoNavigation.Base64
            };
        }
    }
}

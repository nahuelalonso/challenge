﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.BusinessLayer
{
    /// <summary>
    /// Clase que representa las Areas que pueden utilizar las compañias
    /// </summary>
    public class ClsArea
    {
        /// <summary>
        /// Id de la relación entre el Area y la empresa en la base
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Nombre del Area
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Fecha desde para la vigencia del area
        /// </summary>
        public DateTime ValidFrom { get; set; }
        /// <summary>
        /// Fecha Hasta para la vigencia del area, esta puede ser nulla
        /// </summary>
        public DateTime? ValidThru { get; set; }
        /// <summary>
        /// Propiedad que nos dice si actualmente esta o no activa la relación par poder realizar votos en ella
        /// </summary>
        public bool IsActive => Id != 0 && ValidThru == null;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.BusinessLayer
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string objectName, string username) : base($"The {objectName} '{username}' don´t exist") { }
    }
    public class UserPasswordException : Exception
    {
        public UserPasswordException(string username) : base($"The password for user '{username}' is not valid") { }
    }
    public class AlreadyVoteException : Exception
    {
        public AlreadyVoteException(string area) : base($"Ya emitiste un voto en el área '{area}', sólo podes emitir un voto para cada area por mes") { }
    }
    public class UserExistException : Exception
    {
        public UserExistException(string username) : base($"El usuario con username '{username}' ya existe en la base de datos") { }
    }
    public class CompanyAlreadyExistException : Exception
    {
        public CompanyAlreadyExistException(string name) : base($"La compañia con nombre '{name}' ya existe en nuestra base de datos") { }
    }
    public class EmployeeDocketExistException : Exception
    {
        public EmployeeDocketExistException(string docket, long idCompany) : base($"El empleado con legajo '{docket}' para la companyId '{idCompany}' ya existe en nuestra base de datos") { }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.BusinessLayer
{
    public class ClsPhoto
    {
        public long Id { get; set; }
        public DateTime UploadDate { get; set; }
        public string Base64 { get; set; }
    }
}

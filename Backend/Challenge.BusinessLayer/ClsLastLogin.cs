﻿using System;

namespace Challenge.BusinessLayer
{
    public class ClsLastLogin
    {
        public long Id { get; set; }
        public DateTime Date { get; set; }
        public string FromIp { get; set; }
    }
}
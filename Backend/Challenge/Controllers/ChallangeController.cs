﻿using Challenge.Endpoints.Attributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Challenge.Controllers
{
    [Authorize]
    [ApiController]
    [TypeFilter(typeof(ExceptionAttribute))]
    [Route("[controller]")]
    public class ChallengeController : ControllerBase { }
}

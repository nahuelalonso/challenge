﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Challenge.BusinessLayer;
using Challenge.Endpoints;
using Challenge.Endpoints.Attributes;
using Challenge.Endpoints.DTOSetting;
using Challenge.Endpoints.Exceptions;
using Challenge.Endpoints.Extensions;
using Challenge.Endpoints.Parsers;
using Challenge.Endpoints.Request;
using Challenge.Endpoints.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Challenge.Controllers
{
    public class EmployeeController : ChallengeController
    {
        private readonly DataManager.ChallengeRepository Repository;
        private readonly ILogger<EmployeeController> Logger;

        public EmployeeController(
            DataManager.ChallengeRepository repository,
            ILogger<EmployeeController> logger)
        {
            Repository = repository;
            Logger = logger;
        }

        [Authorize(Roles = EndpointConstants.Role.Admin)]
        [HttpPost]
        [Produces(typeof(ChallengeGeneralResponse<EmployeeResponse>))]
        public async Task<IActionResult> CreateEmployee([FromBody] CreateEmployeeRequest request)
        {
            Logger.LogDebug($"Se recibe request de creación de empleado, {Newtonsoft.Json.JsonConvert.SerializeObject(request)}");
            var response = new ChallengeGeneralResponse<EmployeeResponse>();
            try
            {
                await request.Validate(Repository, HttpContext.GetIdCompany());
                var user = new ClsUser(Repository)
                {
                    Username = request.User.Username,
                    Employee = new ClsEmployee(Repository)
                    {
                        Docket = request.Docket,
                        Name = request.Name,
                        Company = new ClsCompany(request.IdCompany, Repository)
                    }
                };
                await user.Create(request.User.Password);
                response.Response = user.Employee.ToResponse();
                response.Response.User = user.ToResponse();
            }
            catch (EmployeeDocketExistException ex)
            {
                Logger.LogDebug($"Ocurrio un error al momento de intentar crear un empleado, {ex}");
                response.Code = StatusCodes.Status400BadRequest;
                response.Errors.Add(new ErrorResponse
                {
                    Code = "EMPLOYEEDOCKETALREADYEXIST",
                    Reason = ex.Message
                });
            }
            catch (UserExistException ex)
            {
                Logger.LogDebug($"Ocurrio un error al momento de intentar crear un empleado, {ex}");
                response.Code = StatusCodes.Status400BadRequest;
                response.Errors.Add(new ErrorResponse
                {
                    Code = "USERALREADYEXIST",
                    Reason = ex.Message
                });
            }
            catch (FieldNullException ex)
            {
                Logger.LogDebug($"Ocurrio un error al momento de intentar crear un empleado, {ex}");
                response.Code = StatusCodes.Status400BadRequest;
                response.Errors.Add(new ErrorResponse
                {
                    Code = "FIELDNOTVALID",
                    Reason = ex.Message
                });
            }
            return StatusCode(response.Code, response);
        }

        [Authorize(Roles = EndpointConstants.Role.Admin + "," +EndpointConstants.Role.Employee)]
        [HttpPost("vote")]
        [Produces(typeof(ChallengeGeneralResponse<VoteRequest>))]
        public async Task<IActionResult> Vote([FromBody] List<VoteRequest> request)
        {
            Logger.LogDebug($"Se recibe request de votación, {Newtonsoft.Json.JsonConvert.SerializeObject(request)}");
            var response = new ChallengeGeneralResponse<string>();
            try
            {
                await request.Validate(HttpContext.GetIdEmployee(), HttpContext.GetIdCompany(), Repository);
                var employee = new ClsEmployee(HttpContext.GetIdEmployee(), Repository);
                await employee.Instanciate(true);
                var votes = new List<ClsVote>();
                foreach (var currentVote in request)
                {
                    var toAdd = new ClsVote()
                    {
                        EmitedDate = DateTime.Now,
                        EmployeeVoted = new ClsEmployee(currentVote.IdEmployee, Repository),
                        Comment = currentVote.Comment,
                        AreaVoted = new ClsArea
                        {
                            Id = currentVote.IdCompanyArea
                        }
                    };
                    votes.Add(toAdd);
                }
                var alreadyVoteArea = await employee.Vote(votes);
                response.Response = "Sus votos fueron emitidos con éxito";
                if (alreadyVoteArea.Count > 0)
                {
                    if (alreadyVoteArea.Count == request.Count)
                    {
                        response.Code = StatusCodes.Status400BadRequest;
                        response.Response = null;
                        response.Errors = new List<ErrorResponse>{
                            new ErrorResponse
                            {
                                Code = "ALREADYALLEMITVOTE",
                                Reason = "Todos los votos que intentas emitir ya fueron emitidos"
                            }
                        };
                    } else {
                        response.Code = StatusCodes.Status206PartialContent;
                        response.Errors = alreadyVoteArea.Select(current => new ErrorResponse
                        {
                            Code = "ALREADYEMITVOTE",
                            Reason = current
                        }).ToList();
                    }
                }
            }
            catch (FieldNullException ex)
            {
                Logger.LogDebug($"Ocurrio un error al momento de intentar crear un empleado, {ex}");
                response.Code = StatusCodes.Status400BadRequest;
                response.Errors.Add(new ErrorResponse
                {
                    Code = "FIELDNOTVALID",
                    Reason = ex.Message
                });
            }
            return StatusCode(response.Code, response);
        }

        [Authorize(Roles = EndpointConstants.Role.Admin + "," + EndpointConstants.Role.Employee)]
        [HttpGet]
        [Produces(typeof(ChallengeGeneralResponse<EmployeeResponse>))]
        public async Task<IActionResult> GetAll()
        {
            Logger.LogDebug($"Se recibe request de obtención de los empleados de la compañia");
            var response = new ChallengeGeneralResponse<List<EmployeeResponse>>();
            var employees = await ClsEmployee.GetAllByCompany(HttpContext.GetIdCompany(), Repository);
            response.Response = employees.ToResponse();
            return StatusCode(response.Code, response);
        }
    }
}

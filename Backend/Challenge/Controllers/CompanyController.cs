﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Challenge.BusinessLayer;
using Challenge.Endpoints;
using Challenge.Endpoints.Attributes;
using Challenge.Endpoints.DTOSetting;
using Challenge.Endpoints.Exceptions;
using Challenge.Endpoints.Extensions;
using Challenge.Endpoints.Parsers;
using Challenge.Endpoints.Request;
using Challenge.Endpoints.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Challenge.Controllers
{
    public class CompanyController : ChallengeController
    {
        private readonly DataManager.ChallengeRepository Repository;
        private readonly ILogger<CompanyController> Logger;

        public CompanyController(
            DataManager.ChallengeRepository repository,
            ILogger<CompanyController> logger)
        {
            Repository = repository;
            Logger = logger;
        }

        [AllowAnonymous]
        [HttpPost]
        [Produces(typeof(ChallengeGeneralResponse<CompanyResponse>))]
        public async Task<IActionResult> CreateCompany([FromBody] CreateCompanyRequest request)
        {
            Logger.LogDebug($"Se recibe request de creación de empresa, {Newtonsoft.Json.JsonConvert.SerializeObject(request)}");
            var response = new ChallengeGeneralResponse<CompanyResponse>();
            try
            {
                await request.Validate(Repository);
                var user = new ClsUser(Repository)
                {
                    Username = request.Employee.User.Username,
                    Employee = new ClsEmployee(Repository)
                    {
                        Docket = request.Employee.Docket,
                        Name = request.Employee.Name,
                        Company = new ClsCompany(Repository)
                        {
                            Name = request.Name,
                            Areas = request.Areas.Distinct().Select(current => new ClsArea { Name = current, ValidFrom = DateTime.Now }).ToList()
                        }
                    }
                };
                await user.Employee.Company.Create();
                await user.Create(request.Employee.User.Password, "Admin");
                response.Response = user.Employee.Company.ToResponse();
                response.Response.Employee = user.Employee.ToResponse();
                response.Response.Employee.User = user.ToResponse();
            }
            catch (CompanyAlreadyExistException ex)
            {
                Logger.LogDebug($"Ocurrio un error al momento de intentar crear una empresa, {ex}");
                response.Code = StatusCodes.Status400BadRequest;
                response.Errors.Add(new ErrorResponse
                {
                    Code = "COMPANYALREADYEXIST",
                    Reason = ex.Message
                });
            }
            catch (EmployeeDocketExistException ex)
            {
                Logger.LogDebug($"Ocurrio un error al momento de intentar crear una empresa, {ex}");
                response.Code = StatusCodes.Status400BadRequest;
                response.Errors.Add(new ErrorResponse
                {
                    Code = "EMPLOYEEDOCKETALREADYEXIST",
                    Reason = ex.Message
                });
            }
            catch (UserExistException ex)
            {
                Logger.LogDebug($"Ocurrio un error al momento de intentar crear una empresa, {ex}");
                response.Code = StatusCodes.Status400BadRequest;
                response.Errors.Add(new ErrorResponse
                {
                    Code = "USERALREADYEXIST",
                    Reason = ex.Message
                });
            }
            catch (FieldNullException ex)
            {
                Logger.LogDebug($"Ocurrio un error al momento de intentar crear una empresa, {ex}");
                response.Code = StatusCodes.Status400BadRequest;
                response.Errors.Add(new ErrorResponse
                {
                    Code = "FIELDNOTVALID",
                    Reason = ex.Message
                });
            }
            return StatusCode(response.Code, response);
        }

        [Authorize(Roles = EndpointConstants.Role.Admin + "," + EndpointConstants.Role.Employee)]
        [HttpGet("areas")]
        [Produces(typeof(ChallengeGeneralResponse<List<AreaResponse>>))]
        public async Task<IActionResult> GetAllAreas()
        {
            Logger.LogDebug($"Se recibe request de obtención de las areas de la empresa");
            var response = new ChallengeGeneralResponse<List<AreaResponse>>();
            var areas = await ClsCompany.GetAllAreas(HttpContext.GetIdCompany(), Repository);
            response.Response = areas.ToResponse();
            return StatusCode(response.Code, response);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Challenge.BusinessLayer;
using Challenge.Endpoints.Attributes;
using Challenge.Endpoints.DTOSetting;
using Challenge.Endpoints.Exceptions;
using Challenge.Endpoints.Extensions;
using Challenge.Endpoints.Request;
using Challenge.Endpoints.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Challenge.Controllers
{
    [Route("auth")]
    public class AuthenticatorController : ChallengeController
    {
        private readonly DataManager.ChallengeRepository Repository;
        private readonly ILogger<AuthenticatorController> Logger;
        private readonly JsonWebTokenSetting JWTSettings;

        public AuthenticatorController(
            DataManager.ChallengeRepository repository,
            ILogger<AuthenticatorController> logger,
            IOptions<JsonWebTokenSetting> jwtSettings)
        {
            Repository = repository;
            JWTSettings = jwtSettings.Value;
            Logger = logger;
        }

        [AllowAnonymous]
        [HttpPost("login")]
        [Produces(typeof(ChallengeGeneralResponse<string>))]
        public async Task<IActionResult> Login([FromBody] LoginRequest request)
        {
            var response = new ChallengeGeneralResponse<string>();
            try
            {
                await request.Validate();
                var user = await ClsUser.Login(request.Username, request.Password, HttpContext.GetIp(), Repository);
                response.Response = user.GetToken(JWTSettings);
            }
            catch (NotFoundException ex)
            {
                throw new UnauthorizedAccessException(ex.ToString());
            }
            catch (UserPasswordException ex)
            {
                throw new UnauthorizedAccessException(ex.ToString());
            }
            catch (FieldNullException ex)
            {
                response.Code = StatusCodes.Status400BadRequest;
                response.Errors.Add(new ErrorResponse
                {
                    Code = "FIELDNOTVALID",
                    Reason = ex.Message
                });
            }
            return StatusCode(response.Code, response);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Challenge.BusinessLayer;
using Challenge.Endpoints;
using Challenge.Endpoints.Attributes;
using Challenge.Endpoints.DTOSetting;
using Challenge.Endpoints.Exceptions;
using Challenge.Endpoints.Extensions;
using Challenge.Endpoints.Parsers;
using Challenge.Endpoints.Request;
using Challenge.Endpoints.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Challenge.Controllers
{
    public class ReportController : ChallengeController
    {
        private readonly DataManager.ChallengeRepository Repository;
        private readonly ILogger<ReportController> Logger;

        public ReportController(
            DataManager.ChallengeRepository repository,
            ILogger<ReportController> logger)
        {
            Repository = repository;
            Logger = logger;
        }

        [Authorize(Roles = EndpointConstants.Role.Admin)]
        [HttpGet("totalEmployees")]
        [Produces(typeof(ChallengeGeneralResponse<int>))]
        public async Task<IActionResult> GetCountEmployee()
        {
            Logger.LogDebug($"Se recibe request de consulta de total de empleados para la empresa");
            var response = new ChallengeGeneralResponse<int>();
            response.Response = await ClsCompany.GetCountEmployees(HttpContext.GetIdCompany(), Repository);
            return StatusCode(response.Code, response);
        }

        [Authorize(Roles = EndpointConstants.Role.Admin)]
        [HttpGet("rankingByArea/from/{monthFrom}-{yearFrom}/to/{monthTo}-{yearTo}")]
        [Produces(typeof(ChallengeGeneralResponse<List<RankingByMonth<RankingByArea>>>))]
        public async Task<IActionResult> GetRankingByArea([FromRoute] int monthFrom, [FromRoute] int yearFrom, [FromRoute] int monthTo, [FromRoute] int yearTo)
        {
            Logger.LogDebug($"Se recibe request de consulta de ranking de los más votados x areas");
            var response = new ChallengeGeneralResponse<List<RankingByMonth<RankingByArea>>>();
            try
            {
                var dateFrom = new DateTime(yearFrom, monthFrom, 1);
                var dateTo = new DateTime(yearTo, monthTo, 1);
                if (dateFrom > dateTo)
                {
                    Logger.LogDebug($"Ocurrio un error al momento de obtener un ranking según el area. La fecha hasta es menor a la fecha desde");
                    response.Code = StatusCodes.Status400BadRequest;
                    response.Errors.Add(new ErrorResponse
                    {
                        Code = "INVALIDDATES",
                        Reason = "El año y mes del fin de período no puede ser menor al año y mes de comienzo del período"
                    });
                }
                else
                {
                    response.Response = await ClsCompany.GetRankingEmployeeByArea(HttpContext.GetIdCompany(), dateFrom, dateTo, Repository);
                }
            } catch(ArgumentOutOfRangeException)
            {
                Logger.LogDebug($"Ocurrio un error al momento de obtener un ranking según el area. Alguna de las fechas esta fuera del rango permitido");
                response.Code = StatusCodes.Status400BadRequest;
                response.Errors.Add(new ErrorResponse
                {
                    Code = "INVALIDDATEOUTOFRANGE",
                    Reason = "Alguna de las fechas esta fuera del rango permitido"
                });
            }
            return StatusCode(response.Code, response);
        }

        [Authorize(Roles = EndpointConstants.Role.Admin)]
        [HttpGet("rankingByMonth/from/{monthFrom}-{yearFrom}/to/{monthTo}-{yearTo}")]
        [Produces(typeof(ChallengeGeneralResponse<List<RankingByMonth<RankingEmployee>>>))]
        public async Task<IActionResult> GetRankingByMonth([FromRoute] int monthFrom, [FromRoute] int yearFrom, [FromRoute] int monthTo, [FromRoute] int yearTo)
        {
            Logger.LogDebug($"Se recibe request de consulta de ranking de los más votados x areas");
            var response = new ChallengeGeneralResponse<List<RankingByMonth<RankingEmployee>>>();
            try
            {
                var dateFrom = new DateTime(yearFrom, monthFrom, 1);
                var dateTo = new DateTime(yearTo, monthTo, 1);
                if (dateFrom > dateTo)
                {
                    Logger.LogDebug($"Ocurrio un error al momento de obtener un ranking según el area. La fecha hasta es menor a la fecha desde");
                    response.Code = StatusCodes.Status400BadRequest;
                    response.Errors.Add(new ErrorResponse
                    {
                        Code = "INVALIDDATES",
                        Reason = "El año y mes del fin de período no puede ser menor al año y mes de comienzo del período"
                    });
                }
                else
                {
                    response.Response = await ClsCompany.GetRankingEmployeeByMonth(HttpContext.GetIdCompany(), dateFrom, dateTo, Repository);
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                Logger.LogDebug($"Ocurrio un error al momento de obtener un ranking según el area. Alguna de las fechas esta fuera del rango permitido");
                response.Code = StatusCodes.Status400BadRequest;
                response.Errors.Add(new ErrorResponse
                {
                    Code = "INVALIDDATEOUTOFRANGE",
                    Reason = "Alguna de las fechas esta fuera del rango permitido"
                });
            }
            return StatusCode(response.Code, response);
        }
    }
}

﻿using Challenge.BusinessLayer;
using Challenge.Endpoints.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.Endpoints.Parsers
{
    /// <summary>
    /// Parser que convierte un ClsUsera un UserResponse
    /// </summary>
    public static class ClsUserToResponse
    {
        public static UserResponse ToResponse(this ClsUser current)
        {
            var toReturn = new UserResponse
            {
                Id = current.Id,
                Username = current.Username,
                RoleName = current.Role
            };
            return toReturn;
        }
    }
}

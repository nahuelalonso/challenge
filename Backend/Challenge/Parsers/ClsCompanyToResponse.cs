﻿using Challenge.BusinessLayer;
using Challenge.Endpoints.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.Endpoints.Parsers
{
    /// <summary>
    /// Parser que convierte un ClsCompany a un CompanyResponse
    /// </summary>
    public static class ClsCompanyToResponse
    {
        public static CompanyResponse ToResponse(this ClsCompany current)
        {
            var toReturn = new CompanyResponse
            {
                Id = current.Id,
                Name = current.Name,
                Areas = new List<AreaResponse>()
            };
            if ((current.Areas?.Count ?? 0) > 0)
                toReturn.Areas = current.Areas.ToResponse();
            return toReturn;
        }
    }
}

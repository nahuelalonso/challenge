﻿using Challenge.BusinessLayer;
using Challenge.Endpoints.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.Endpoints.Parsers
{
    /// <summary>
    /// Parser que convierte un ClsEmployee a un EmployeeResponse
    /// </summary>
    public static class ClsEmployeeToResponse
    {
        public static List<EmployeeResponse> ToResponse(this List<ClsEmployee> current)
        {
            return current.Select(current => current.ToResponse()).ToList();
        }
        public static EmployeeResponse ToResponse(this ClsEmployee current)
        {
            var toReturn = new EmployeeResponse
            {
                Id = current.Id,
                Name = current.Name,
                Docket = current.Docket
            };
            return toReturn;
        }
    }
}

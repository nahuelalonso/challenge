﻿using Challenge.BusinessLayer;
using Challenge.Endpoints.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.Endpoints.Parsers
{
    /// <summary>
    /// Parser que convierte un ClsUsera un UserResponse
    /// </summary>
    public static class ClsAreaToResponse
    {
        public static List<AreaResponse> ToResponse(this List<ClsArea> areas)
        {
            return areas.Select(current => current.ToResponse()).ToList();
        }
        public static AreaResponse ToResponse(this ClsArea current)
        {
            return new AreaResponse
            {
                Id = current.Id,
                Name = current.Name
            };
        }
    }
}

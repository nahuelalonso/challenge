﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.Endpoints.Exceptions
{
    public class ForbiddenException : Exception
    {
        public ForbiddenException() : base("You don´t have permission") { }
    }
    public class FieldNullException : Exception
    {
        public FieldNullException(string msg) : base(msg) { }
    }
    public class ClaimNotFoundException : Exception
    {
        public ClaimNotFoundException(string claimName) : base($"No pudimos obtener los datos del token para la key: '{claimName}'") { }
    }
}

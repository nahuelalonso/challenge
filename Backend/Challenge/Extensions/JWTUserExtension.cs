﻿using Challenge.BusinessLayer;
using Challenge.Endpoints.DTOSetting;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Challenge.Endpoints.Extensions
{
    /// <summary>
    /// Extensión encargada de obtener el JWT para devolver
    /// </summary>
    public static class JWTUserExtension
    {
        /// <summary>
        /// Extiende el usuario y genera un JWT
        /// </summary>
        /// <param name="user">Usuario</param>
        /// <param name="jwtSecret">SecretKey de JWT</param>
        /// <param name="minutesOfExpired">Minutos en los que expira el token</param>
        /// <returns></returns>
        public static string GetToken(this ClsUser user, JsonWebTokenSetting jwtSetting)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.UTF8.GetBytes(jwtSetting.SecretKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(EndpointConstants.ClaimsName.IdUser, user.Id.ToString()),
                    new Claim(EndpointConstants.ClaimsName.Username, user.Username),
                    new Claim(EndpointConstants.ClaimsName.IdEmployee, user.Employee.Id.ToString()),
                    new Claim(EndpointConstants.ClaimsName.EmployeeName, user.Employee.Name),
                    new Claim(EndpointConstants.ClaimsName.IdCompany, user.Employee.Company.Id.ToString()),
                    new Claim(EndpointConstants.ClaimsName.CompanyName, user.Employee.Company.Name),
                    new Claim(ClaimTypes.Role, user.Role)
                }),
                Expires = DateTime.UtcNow.AddMinutes(jwtSetting.ExpiredInMinutes),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}

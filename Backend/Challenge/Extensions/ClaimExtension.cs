﻿using Challenge.Endpoints.Exceptions;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.Endpoints.Extensions
{
    public static class ClaimExtension
    {
        public static long GetUserId(this HttpContext context)
        {
            try
            {
                var claims = context.User.Claims.ToList();
                return long.Parse(claims.First(claim => claim.Type == EndpointConstants.ClaimsName.IdUser).Value);
            }
            catch (Exception)
            {
                throw new ClaimNotFoundException(EndpointConstants.ClaimsName.IdUser);
            }
        }

        public static string GetUsername(this HttpContext context)
        {
            try
            {
                var claims = context.User.Claims.ToList();
                return claims.First(claim => claim.Type == EndpointConstants.ClaimsName.Username).Value;
            }
            catch (Exception)
            {
                throw new ClaimNotFoundException(EndpointConstants.ClaimsName.Username);
            }
        }

        public static long GetIdCompany(this HttpContext context)
        {
            try
            {
                var claims = context.User.Claims.ToList();
                return long.Parse(claims.First(claim => claim.Type == EndpointConstants.ClaimsName.IdCompany).Value);
            }
            catch (Exception)
            {
                throw new ClaimNotFoundException(EndpointConstants.ClaimsName.IdCompany);
            }
        }

        public static string GetCompanyName(this HttpContext context)
        {
            try
            {
                var claims = context.User.Claims.ToList();
                return claims.First(claim => claim.Type == EndpointConstants.ClaimsName.CompanyName).Value;
            }
            catch (Exception)
            {
                throw new ClaimNotFoundException(EndpointConstants.ClaimsName.CompanyName);
            }
        }

        public static long GetIdEmployee(this HttpContext context)
        {
            try
            {
                var claims = context.User.Claims.ToList();
                return long.Parse(claims.First(claim => claim.Type == EndpointConstants.ClaimsName.IdEmployee).Value);
            }
            catch (Exception)
            {
                throw new ClaimNotFoundException(EndpointConstants.ClaimsName.IdEmployee);
            }
        }

        public static string GetEmployeeName(this HttpContext context)
        {
            try
            {
                var claims = context.User.Claims.ToList();
                return claims.First(claim => claim.Type == EndpointConstants.ClaimsName.EmployeeName).Value;
            }
            catch (Exception)
            {
                throw new ClaimNotFoundException(EndpointConstants.ClaimsName.EmployeeName);
            }
        }

        public static string GetIp(this HttpContext context)
        {
            return context.Connection.RemoteIpAddress.ToString();
        }
    }
}

﻿using Challenge.Endpoints.Exceptions;
using Challenge.Endpoints.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.Endpoints.Attributes
{
    public class ExceptionAttribute : ExceptionFilterAttribute
    {
        private readonly ILogger<ExceptionAttribute> _logger;
        public ExceptionAttribute(ILogger<ExceptionAttribute> logger)
        {
            _logger = logger;
        }
        public override void OnException(ExceptionContext context)
        {
            var response = new ChallengeGeneralResponse<string>();
            if (context.Exception is UnauthorizedAccessException)
            {
                response.Code = StatusCodes.Status401Unauthorized;
                response.Errors.Add(
                    new ErrorResponse { Code = "USERPASSWORDINCORRECT", Reason = "El usuario y/o contraseña son incorrectos" }
                    );
            } else if (context.Exception is ForbiddenException)
            {
                response.Code = StatusCodes.Status403Forbidden;
                response.Errors.Add(
                    new ErrorResponse { Code = "NOTAUTHORIZED", Reason = "Usted no tiene los permisos suficientes para realizar esta petición" }
                    );
            }
            else if (context.Exception is Exception)
            {
                _logger.LogCritical(context.Exception.Message);
                response.Code = StatusCodes.Status500InternalServerError;
                response.Errors.Add(
                    new ErrorResponse { Code = "INTERNALERROR", Reason = "Hubo un problema en el servidor, si esto sigue ocurriendo comuniquese con el administrador" }
                    );
            }
            if (response.Code != StatusCodes.Status200OK)
            {
                context.HttpContext.Response.StatusCode = response.Code;
                context.Result = new JsonResult(response);
            }
            base.OnException(context);
        }
    }
}

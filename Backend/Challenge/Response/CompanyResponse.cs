﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.Endpoints.Response
{
    public class CompanyResponse
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public List<AreaResponse> Areas { get; set; }
        public EmployeeResponse Employee { get; set; }
    }
}

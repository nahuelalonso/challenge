﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.Endpoints.Response
{
    /// <summary>
    /// Respuesta General para todo los endpoint
    /// </summary>
    /// <typeparam name="TypeGeneralResponse">Tipo de Dato que contiene Response</typeparam>
    public class ChallengeGeneralResponse<TypeGeneralResponse>
    {
        public int Code { get; set; } = StatusCodes.Status200OK;
        public string Status
        {
            get
            {
                var toReturn = "OK";
                switch (Code)
                {
                    case 100:
                        toReturn = "Continue";
                        break;
                    case 101:
                        toReturn = "Switching Protocols";
                        break;
                    case 102:
                        toReturn = "Processing";
                        break;
                    case 103:
                        toReturn = "Checkpoint";
                        break;
                    case 200:
                        toReturn = "OK";
                        break;
                    case 201:
                        toReturn = "Created";
                        break;
                    case 202:
                        toReturn = "Accepted";
                        break;
                    case 203:
                        toReturn = "Non-Authoritative Information";
                        break;
                    case 204:
                        toReturn = "No Content";
                        break;
                    case 205:
                        toReturn = "Reset Content";
                        break;
                    case 206:
                        toReturn = "Partial Content";
                        break;
                    case 207:
                        toReturn = "Multi-Status";
                        break;
                    case 208:
                        toReturn = "Already Reported";
                        break;
                    case 300:
                        toReturn = "Multiple Choices";
                        break;
                    case 301:
                        toReturn = "Moved Permanently";
                        break;
                    case 302:
                        toReturn = "Found";
                        break;
                    case 303:
                        toReturn = "See Other";
                        break;
                    case 304:
                        toReturn = "Not Modified";
                        break;
                    case 305:
                        toReturn = "Use Proxy";
                        break;
                    case 306:
                        toReturn = "Switch Proxy";
                        break;
                    case 307:
                        toReturn = "Temporary Redirect";
                        break;
                    case 308:
                        toReturn = "Permanent Redirect";
                        break;
                    case 400:
                        toReturn = "Bad Request";
                        break;
                    case 401:
                        toReturn = "Unauthorized";
                        break;
                    case 402:
                        toReturn = "Payment Required";
                        break;
                    case 403:
                        toReturn = "Forbidden";
                        break;
                    case 404:
                        toReturn = "Not Found";
                        break;
                    case 405:
                        toReturn = "Method Not Allowed";
                        break;
                    case 406:
                        toReturn = "Not Acceptable";
                        break;
                    case 407:
                        toReturn = "Proxy Authentication Required";
                        break;
                    case 408:
                        toReturn = "Request Timeout";
                        break;
                    case 409:
                        toReturn = "Conflict";
                        break;
                    case 410:
                        toReturn = "Gone";
                        break;
                    case 411:
                        toReturn = "Length Required";
                        break;
                    case 412:
                        toReturn = "Precondition Failed";
                        break;
                    case 413:
                        toReturn = "Request Entity Too Large";
                        break;
                    case 414:
                        toReturn = "Request-URI Too Long";
                        break;
                    case 415:
                        toReturn = "Unsupported Media Type";
                        break;
                    case 416:
                        toReturn = "Requested Range Not Satisfiable";
                        break;
                    case 417:
                        toReturn = "Expectation Failed";
                        break;
                    case 418:
                        toReturn = "I'm a teapot";
                        break;
                    case 422:
                        toReturn = "Unprocessable Entity";
                        break;
                    case 423:
                        toReturn = "Locked";
                        break;
                    case 424:
                        toReturn = "Failed Dependency";
                        break;
                    case 425:
                        toReturn = "Unassigned";
                        break;
                    case 426:
                        toReturn = "Upgrade Required";
                        break;
                    case 428:
                        toReturn = "Precondition Required";
                        break;
                    case 429:
                        toReturn = "Too Many Requests";
                        break;
                    case 431:
                        toReturn = "Request Header Fields Too Large";
                        break;
                    case 449:
                        toReturn = "";
                        break;
                    case 451:
                        toReturn = "Unavailable for Legal Reasons";
                        break;
                    case 500:
                        toReturn = "Internal Server Error";
                        break;
                    case 501:
                        toReturn = "Not Implemented";
                        break;
                    case 502:
                        toReturn = "Bad Gateway";
                        break;
                    case 503:
                        toReturn = "Service Unavailable";
                        break;
                    case 504:
                        toReturn = "Gateway Timeout";
                        break;
                    case 505:
                        toReturn = "HTTP Version Not Supported";
                        break;
                    case 506:
                        toReturn = "Variant Also Negotiates";
                        break;
                    case 507:
                        toReturn = "Insufficient Storage";
                        break;
                    case 508:
                        toReturn = "Loop Detected";
                        break;
                    case 509:
                        toReturn = "Bandwidth Limit Exceeded";
                        break;
                    case 510:
                        toReturn = "Not Extended";
                        break;
                    case 511:
                        toReturn = "Network Authentication Required";
                        break;
                    case 512:
                        toReturn = "Not updated";
                        break;
                    case 521:
                        toReturn = "Version Mismatch";
                        break;
                }
                return toReturn;
            }
        }
        public List<ErrorResponse> Errors { get; set; } = new List<ErrorResponse>();
        public TypeGeneralResponse Response { get; set; }
    }

    /// <summary>
    /// Estructura para devolver los errores en las respuestas de los endpoints
    /// </summary>
    public class ErrorResponse
    {
        public string Code { get; set; }
        public string Reason { get; set; }
    }
}

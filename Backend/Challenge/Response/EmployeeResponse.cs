﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.Endpoints.Response
{
    public class EmployeeResponse
    {
        public long Id { get; set; }
        public string Docket { get; set; }
        public string Name { get; set; }
        public UserResponse User { get; set; }
    }
}

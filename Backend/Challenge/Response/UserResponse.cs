﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.Endpoints.Response
{
    public class UserResponse
    {
        public long Id { get; set; }
        public string Username { get; set; }
        public string RoleName { get; set; }
    }
}

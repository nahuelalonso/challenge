﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.Endpoints
{
    public static class EndpointConstants
    {
        public static class ClaimsName
        {
            public const string Username = "username";
            public const string IdUser = "idUser";
            public const string IdCompany = "idCompany";
            public const string CompanyName = "companyName";
            public const string IdEmployee = "idEmployee";
            public const string EmployeeName = "employeeName";
        }

        public static class Role
        {
            public const string Admin = "Admin";
            public const string Employee = "Employee";
        }
    }
}

﻿using Challenge.Endpoints.ServiceConfigurator;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.Endpoints.ServiceConfiguration
{
    public static class InitConfig
    {
        public static IServiceCollection Initializer(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddControllers();
            return services
                .CorsConfigurate()
                .RepositoryConfigurate()
                .PostgreSQLConfigurate(configuration)
                .JsonWebTokenConfigurate(configuration);
        }
    }
}

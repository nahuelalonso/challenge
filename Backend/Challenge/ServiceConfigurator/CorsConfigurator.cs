﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.Endpoints.ServiceConfigurator
{

    public static class CorsConfigurator
    {
        public static IServiceCollection CorsConfigurate(this IServiceCollection services) =>
            services.AddCors(options =>
            {
                options.AddPolicy(
                    "AllowAll", builder =>
                    {
                        builder
                                .AllowAnyOrigin()
                                .AllowAnyMethod()
                                .AllowAnyHeader();
                    });
            });
    }
}

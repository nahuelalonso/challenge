﻿using Challenge.DataModelMapper;
using EFCore.DbContextFactory.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Challenge.Endpoints.ServiceConfiguration
{
    public static class PostgreSQLConfigurator
    {
        public static IServiceCollection PostgreSQLConfigurate(this IServiceCollection services, IConfiguration configuration)
        {
            var postgreSQLSection = configuration.GetSection("PostgreSQL");
            services.Configure<DataModelMapper.PostgreSQLConfig>(postgreSQLSection);
            var postgresSQLSettings = postgreSQLSection.Get<DataModelMapper.PostgreSQLConfig>();
            var serviceProvider = new ServiceCollection().AddEntityFrameworkNpgsql().BuildServiceProvider();
            services.AddDbContextFactory<ChallengeContext>(builder => builder.UseNpgsql(postgresSQLSettings.ConnectionString).UseInternalServiceProvider(serviceProvider));
            return services;
        }
    }
}

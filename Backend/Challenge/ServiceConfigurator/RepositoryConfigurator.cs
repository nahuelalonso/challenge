﻿using Challenge.DataManager;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.Endpoints.ServiceConfigurator
{
    public static class RepositoryConfigurator
    {
        public static IServiceCollection RepositoryConfigurate(this IServiceCollection services)
        {
            services.AddScoped<ChallengeRepository>();
            return services;
        }
    }
}

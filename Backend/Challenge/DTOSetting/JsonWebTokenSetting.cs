﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.Endpoints.DTOSetting
{
    public class JsonWebTokenSetting
    {
        public string SecretKey { get; set; } = "MwEF6HV1c3li9gN1tui94AfityTYkMrUNFvBxWnqeJQ17AKoTzLLSCYYHzMXr1mFF2hlmzzfQDeWYDbAjg8qwO7Z0M2sW4hWhHs2WaHqEphQ1ODMXNXGh8hj";
        public int ExpiredInMinutes { get; set; } = 60;
    }
}

﻿using Challenge.Endpoints.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.Endpoints.Request
{
    /// <summary>
    /// Estructura del Request del endpoint de Login
    /// </summary>
    public class LoginRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    /// <summary>
    /// Extensión que valida los datos del request del login
    /// </summary>
    public static class ValidateLoginRequest
    {
        /// <summary>
        /// Validador
        /// </summary>
        /// <param name="request">request a validar</param>
        /// <param name="repository">repositorio para conectarse a la base</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="UserExistException"></exception>
        public async static Task Validate(this LoginRequest request)
        {
            if (string.IsNullOrEmpty(request.Username))
                throw new FieldNullException($"[NULLARGUMENT] - El nombre de usuario no puede ser nulo");
            if (string.IsNullOrEmpty(request.Password))
                throw new FieldNullException($"[NULLARGUMENT] - El password del usuario no puede ser nulo");
            if (request.Password.Length < 6)
                throw new FieldNullException($"[PASSWORDMINLENGTH] - El password del usuario debe contener al menos 6 caracteres");
        }
    }
}

﻿using Challenge.BusinessLayer;
using Challenge.DataManager;
using Challenge.Endpoints.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.Endpoints.Request
{
    /// <summary>
    /// Estructura del Request para crear un Usuario
    /// </summary>
    public class CreateUserRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    /// <summary>
    /// Extensión que valida los datos del request del usuario
    /// </summary>
    public static class ValidateCreateUserRequest
    {
        /// <summary>
        /// Validador
        /// </summary>
        /// <param name="request">request a validar</param>
        /// <param name="repository">repositorio para conectarse a la base</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="UserExistException"></exception>
        public async static Task Validate(this CreateUserRequest request, ChallengeRepository repository)
        {
            if (string.IsNullOrEmpty(request.Username))
                throw new FieldNullException($"[NULLARGUMENT] - El nombre de usuario no puede ser nulo");
            if (string.IsNullOrEmpty(request.Password))
                throw new FieldNullException($"[NULLARGUMENT] - El password del usuario no puede ser nulo");
            if (request.Password.Length < 6)
                throw new FieldNullException($"[PASSWORDMINLENGTH] - El password del usuario debe contener al menos 6 caracteres");
            if (await ClsUser.Exist(request.Username, repository))
                throw new UserExistException(request.Username);
        }
    }
}

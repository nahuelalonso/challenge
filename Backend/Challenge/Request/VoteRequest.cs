﻿using Challenge.BusinessLayer;
using Challenge.DataManager;
using Challenge.Endpoints.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.Endpoints.Request
{
    /// <summary>
    /// Estructura del request para la emisión del Voto
    /// </summary>
    public class VoteRequest
    {
        public long IdEmployee { get; set; }
        public long IdCompanyArea { get; set; }
        public string Comment { get; set; }
    }

    /// <summary>
    /// Extensión que valida los datos del request
    /// </summary>
    public static class ValidateVoteRequest
    {
        /// <summary>
        /// Validador de una lista
        /// </summary>
        /// <param name="request">request a validar</param>
        /// <param name="repository">repositorio para conectarse a la base</param>
        /// <param name="validateCompany">parametro para validar el idCompany</param>
        /// <exception cref="ArgumentNullException"></exception>
        public async static Task Validate(this List<VoteRequest> request, long idEmployeeVoter, long idCompany, ChallengeRepository repository)
        {
            if (request.Count == 0)
                throw new FieldNullException($"[WITHOUTCOLLECTION] - Al menos debe haber un voto");
            foreach (var currentVote in request)
            {
                await currentVote.Validate(idEmployeeVoter, idCompany, repository);
            }
        }

        /// <summary>
        /// Validador Unario
        /// </summary>
        /// <param name="request">request a validar</param>
        /// <param name="repository">repositorio para conectarse a la base</param>
        /// <param name="validateCompany">parametro para validar el idCompany</param>
        /// <exception cref="ArgumentNullException"></exception>
        public async static Task Validate(this VoteRequest request, long idEmployeeVoter, long idCompany, ChallengeRepository repository)
        {
            if (request.IdEmployee == 0)
                throw new FieldNullException($"[NULLARGUMENT] - El nombre del empleado no puede ser nulo");
            if (request.IdEmployee == idEmployeeVoter)
                throw new FieldNullException($"[SELFVOTEINVALID] - No puedes emitir un voto hacia vos mismo");
            if (request.IdCompanyArea == 0)
                throw new FieldNullException($"[NULLARGUMENT] - El legajo del empleado no puede ser nulo");
            if (!await ClsCompany.HasArea(idCompany, request.IdCompanyArea, repository))
                throw new FieldNullException($"[AREANOTVALID] - El area en la que intentas emitir un voto no esta vigente o no existe en la empresa");
            var employee = new ClsEmployee(request.IdEmployee, repository);
            try
            {
                await employee.Instanciate();
                if (employee.Company.Id != idCompany)
                    throw new FieldNullException($"[EMPLOYEENOTWORKINCOMPANY] - El empleado al que intentas votar no trabaja en tu empresa");
            } catch (NotFoundException)
            {
                throw new FieldNullException($"[EMPLOYEENOTEXIST] - El empleado al que intentas votar no existe en nuestra base");
            }
        }
    }
}

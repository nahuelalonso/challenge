﻿using Challenge.BusinessLayer;
using Challenge.DataManager;
using Challenge.Endpoints.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.Endpoints.Request
{
    /// <summary>
    /// 
    /// </summary>
    public class CreateCompanyRequest
    {
        public string Name { get; set; }
        public List<string> Areas { get; set; }
        public CreateEmployeeRequest Employee { get; set; }
    }

    public static class ValidateCreateCompanyRequest
    {
        public async static Task Validate(this CreateCompanyRequest request, ChallengeRepository repository)
        {
            if (string.IsNullOrEmpty(request.Name))
                throw new FieldNullException($"[NULLARGUMENT] - El nombre de la empresa no puede ser nulo");
            if (request.Areas.Count > 0 && request.Areas.Any(current => string.IsNullOrEmpty(current)))
                throw new FieldNullException($"[NULLARGUMENT] - El nombre de las areas no puede ser nulo");
            if (request.Employee == null)
                throw new FieldNullException($"[NULLARGUMENT] - La estructura Employee no debe ser nula");
            if (await ClsCompany.Exist(request.Name, repository))
                throw new CompanyAlreadyExistException(request.Name);
            await request.Employee.Validate(repository);
        }
    }
}

﻿using Challenge.BusinessLayer;
using Challenge.DataManager;
using Challenge.Endpoints.Exceptions;
using Challenge.Endpoints.Extensions;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Challenge.Endpoints.Request
{
    /// <summary>
    /// Estructura del request para la creación de un empleado
    /// </summary>
    public class CreateEmployeeRequest
    {
        public string Docket { get; set; }
        public string Name { get; set; }
        public long IdCompany { get; set; }
        public CreateUserRequest User { get; set; }
    }

    /// <summary>
    /// Extensión que valida los datos del request
    /// </summary>
    public static class ValidateCreateEmployeeRequest
    {
        /// <summary>
        /// Validador
        /// </summary>
        /// <param name="request">request a validar</param>
        /// <param name="repository">repositorio para conectarse a la base</param>
        /// <param name="idCompany">id de la compañia en la que el empleado se va a crear</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="EmployeeDocketExistException"></exception>
        /// <exception cref="UserExistException"></exception>
        public async static Task Validate(this CreateEmployeeRequest request, ChallengeRepository repository, long idCompany = 0)
        {
            if (string.IsNullOrEmpty(request.Name))
                throw new FieldNullException($"[NULLARGUMENT] - El nombre del empleado no puede ser nulo");
            if (string.IsNullOrEmpty(request.Docket))
                throw new FieldNullException($"[NULLARGUMENT] - El legajo del empleado no puede ser nulo");
            if (idCompany != 0)
            {
                request.IdCompany = idCompany;
                if (await ClsEmployee.Exist(request.Docket, request.IdCompany, repository))
                    throw new EmployeeDocketExistException(request.Docket, request.IdCompany);
            }
            if (request.User == null)
                throw new FieldNullException($"[NULLARGUMENT] - La estructura User no debe ser nula");
            await request.User.Validate(repository);
        }
    }
}

﻿using Challenge.DataModelMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Challenge.DataManager.Models
{
    /// <summary>
    /// Funciones permitidas contra la base de datos para todo lo que corresponde a la tabla Role
    /// </summary>
    public class RoleModel
    {
        private readonly Func<ChallengeContext> Factory;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="factory">Factory del ChallengeContext</param>
        public RoleModel(Func<ChallengeContext> factory)
        {
            Factory = factory;
        }

        /// <summary>
        /// Función para agregar un role en la base
        /// </summary>
        /// <param name="toAdd">Objeto Role a incorporar en la base</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        /// <returns>Objeto Role incorporado con su Id</returns>
        public async Task<Role> Add(Role toAdd, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                _ = await context.Role.AddAsync(Sanitize(toAdd), ct);
                _ = await context.SaveChangesAsync(ct);
                return toAdd;
            }
        }

        /// <summary>
        /// Función para obtener un rol según el nombre
        /// </summary>
        /// <param name="name">Nombre del rol a buscar</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        /// <returns>Role que se encuentra en la base de datos</returns>
        public async Task<Role> GetByName(string name, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                return await context.Role.FirstOrDefaultAsync(current => current.Name.ToLower() == name.ToLower().Trim(), ct);
            }
        }

        private Role Sanitize(Role toSanitize)
        {
            toSanitize.Name = toSanitize.Name.Trim();
            return toSanitize;
        }
    }
}

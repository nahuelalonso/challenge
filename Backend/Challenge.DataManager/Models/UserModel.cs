﻿using Challenge.DataModelMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Challenge.DataManager.Models
{
    /// <summary>
    /// Funciones permitidas contra la base de datos para todo lo que corresponde a la tabla UserOfSystem
    /// </summary>
    public class UserModel
    {
        private readonly Func<ChallengeContext> Factory;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="factory">Factory del ChallengeContext</param>
        public UserModel(Func<ChallengeContext> factory)
        {
            Factory = factory;
        }

        /// <summary>
        /// Función para agregar un usuario en la base
        /// </summary>
        /// <param name="toAdd">Objeto UserOfSystem a incorporar en la base</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        /// <returns>Objeto UserOfSystem incorporado con su Id</returns>
        public async Task<UserOfSystem> Add(UserOfSystem toAdd, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                _ = await context.UserOfSystem.AddAsync(Sanitize(toAdd), ct);
                _ = await context.SaveChangesAsync(ct);
                return toAdd;
            }
        }

        /// <summary>
        /// Función para traer de la base de datos el usuario segú su username
        /// </summary>
        /// <param name="username">Username a buscar</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        /// <returns>Usuario guardado en base</returns>
        public async Task<UserOfSystem> GetByUsername(string username, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                return await context.UserOfSystem.Include(current => current.IdRoleNavigation).FirstOrDefaultAsync(current => current.Username.ToLower() == username.ToLower().Trim(), ct);
            }
        }

        /// <summary>
        /// Función para actualizar los datos del usuario en base
        /// </summary>
        /// <remarks>El Username no puede ser actualizado</remarks>
        /// <param name="toUpdate">Objeto UserOfSystem a actualizar</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        public async Task Update(UserOfSystem toUpdate, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                var current = await context.UserOfSystem.FirstOrDefaultAsync(current => current.Id == toUpdate.Id, ct);
                current.IdEmployee = toUpdate.IdEmployee;
                current.IdRole = toUpdate.IdRole;
                current.PasswordEncrypted = toUpdate.PasswordEncrypted;
                _ = await context.SaveChangesAsync(ct);
            }
        }

        /// <summary>
        /// Función para agregar el acceso histórico del usuario
        /// </summary>
        /// <param name="toAdd">Objeto UserLogin para incorporar en la base</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        public async Task AddLastLogin(UserLogin toAdd, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                _ = await context.UserLogin.AddAsync(toAdd, ct);
                _ = await context.SaveChangesAsync(ct);
            }
        }

        private UserOfSystem Sanitize(UserOfSystem toSanitize)
        {
            toSanitize.Username = toSanitize.Username.Trim();
            toSanitize.PasswordEncrypted = toSanitize.PasswordEncrypted.Trim();
            return toSanitize;
        }
    }
}

﻿using Challenge.DataModelMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Challenge.DataManager.Models
{
    /// <summary>
    /// Funciones permitidas contra la base de datos para todo lo que corresponde a la tabla Company
    /// </summary>
    public class CompanyModel
    {
        private readonly Func<ChallengeContext> Factory;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="factory">Factory del ChallengeContext</param>
        public CompanyModel(Func<ChallengeContext> factory)
        {
            Factory = factory;
        }

        /// <summary>
        /// Función para agregar una compañia en la base
        /// </summary>
        /// <param name="toAdd">Objeto Company a incorporar en la base</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        /// <returns>Objeto Company incorporado con su Id</returns>
        public async Task<Company> Add(Company toAdd, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                _ = await context.Company.AddAsync(Sanitize(toAdd), ct);
                _ = await context.SaveChangesAsync(ct);
                return toAdd;
            }
        }

        /// <summary>
        /// Función para obtener la compañia que coincidan con el nombre ingresado
        /// </summary>
        /// <param name="name">Nombre a buscar</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        /// <returns>Compañia</returns>
        public async Task<Company> GetByName(string name, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                return await context.Company.FirstOrDefaultAsync(current => current.Name.ToLower() == name.ToLower().Trim(), ct);
            }
        }

        /// <summary>
        /// Función para obtener una compañia según su id en base
        /// </summary>
        /// <param name="id">Id de la compañia</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        /// <returns>Compañia del id buscado</returns>
        public async Task<Company> GetById(long id, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                return await context.Company.FirstOrDefaultAsync(current => current.Id == id, ct);
            }
        }

        /// <summary>
        /// Fución para agregar un area a la compañia deseada
        /// </summary>
        /// <param name="idCompany">Id de la compañia a la que se le ingresa el área</param>
        /// <param name="idArea">Id del area a ingresar</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        /// <returns>Devuleve el objeto CompanyArea recien Agregado</returns>
        public async Task<CompanyArea> AddCompanyArea(long idCompany, long idArea, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                var toAdd = new CompanyArea
                {
                    IdArea = idArea,
                    IdCompany = idCompany,
                    ValidFrom = DateTime.Now
                };
                _ = await context.CompanyArea.AddAsync(toAdd, ct);
                _ = await context.SaveChangesAsync(ct);
                return toAdd;
            }
        }

        /// <summary>
        /// Fución para obtener un area a la compañia deseada
        /// </summary>
        /// <param name="idCompany">Id de la compañia a la que se le ingresa el área</param>
        /// <param name="idArea">Id del area a ingresar</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        /// <returns>Obtiene el companyArea si esta activo</returns>
        public async Task<CompanyArea> GetCompanyArea(long idCompany, long idArea, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                return await context.CompanyArea
                                    .OrderByDescending(companyArea => companyArea.ValidFrom)
                                    .FirstOrDefaultAsync(current => 
                                        current.IdCompany == idCompany
                                        && current.IdArea == idArea
                                        && current.ValidThru == null, ct);
            }
        }

        /// <summary>
        /// Función para cancelar el area en la compañia
        /// </summary>
        /// <param name="idCompany">Id de la compañia</param>
        /// <param name="idArea">Id del área a cancelar</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        public async Task CancelCompanyArea(long idCompany, long idArea, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                var current = await context.CompanyArea
                                    .OrderByDescending(companyArea => companyArea.ValidFrom)
                                    .FirstOrDefaultAsync(current => current.IdCompany == idCompany && current.IdArea == idArea, ct);
                if (current == null) return;
                current.ValidThru = DateTime.Now;
                _ = await context.SaveChangesAsync(ct);
            }
        }

        /// <summary>
        /// Función para obtener la lista de las areas de la compañia
        /// </summary>
        /// <param name="idCompany">Id de la compañia</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        /// <returns>Lista de CompanyAreas para la compañia seleccionada</returns>
        public async Task<List<CompanyArea>> GetCompanyAreas(long idCompany, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                return await context.CompanyArea
                                        .OrderByDescending(companyArea => companyArea.Id)
                                        .Include(current => current.IdAreaNavigation)
                                        .Where(current => current.IdCompany == idCompany && current.ValidFrom < DateTime.Now && current.ValidThru == null)
                                        .ToListAsync(ct);
            }
        }

        /// <summary>
        /// Función para obtener la cantidad de empleados de la compañia
        /// </summary>
        /// <param name="idCompany">Id de la compañia a obtener la cantidad de empleados</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        /// <returns>Total de empleados de la empresa</returns>
        public async Task<int> CountEmployee(long idCompany, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                return await context.Employee.Where(current => current.IdCompany == idCompany).CountAsync(ct);
            }
        }

        private Company Sanitize(Company toSanitize)
        {
            toSanitize.Name = toSanitize.Name.Trim();
            return toSanitize;
        }
    }
}

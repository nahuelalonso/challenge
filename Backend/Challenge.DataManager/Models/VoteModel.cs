﻿using Challenge.DataModelMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Challenge.DataManager.Models
{
    /// <summary>
    /// Funciones permitidas contra la base de datos para todo lo que corresponde a la tabla Vote
    /// </summary>
    public class VoteModel
    {
        private readonly Func<ChallengeContext> Factory;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="factory">Factory del ChallengeContext</param>
        public VoteModel(Func<ChallengeContext> factory)
        {
            Factory = factory;
        }

        /// <summary>
        /// Función para agregar un voto en la base
        /// </summary>
        /// <param name="toAdd">Objeto Voto que se desea guardas, puede o no contener el comentario</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        /// <returns>Devuelve el objeto creado junto con su Id</returns>
        public async Task<Vote> Add(Vote toAdd, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                _ = await context.Vote.AddAsync(Sanitize(toAdd), ct);
                _ = await context.SaveChangesAsync(ct);
                return toAdd;
            }
        }

        /// <summary>
        /// Función para ir a buscar todos los votos dentro de un rango de fechas para una compañia
        /// </summary>
        /// <param name="idCompany">Id de la compañia</param>
        /// <param name="dateFrom">Fecha desde</param>
        /// <param name="dateTo">Fecha Hasta</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        /// <returns>Lista de votos dentro del rango de fechas para la compañia seleccionada, junto con sus comentarios si los tuviese</returns>
        public async Task<List<Vote>> GetByDateRangeForCompany(long idCompany, DateTime dateFrom, DateTime dateTo, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                return await context.Vote
                                .Include(vote => vote.VoteComment)
                                .Where(current =>
                                    current.IdCompanyAreaNavigation.IdCompany == idCompany
                                    && current.Date >= dateFrom
                                    && current.Date < dateTo)
                                .ToListAsync(ct);
            }
        }

        /// <summary>
        /// Función para ir a buscar todos los votos dentro de un rango de fechas de un empleado
        /// </summary>
        /// <param name="idEmployee">Id del empleado votante</param>
        /// <param name="dateFrom">Fecha desde</param>
        /// <param name="dateTo">Fecha Hasta</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        /// <returns>Lista de votos dentro del rango de fechas para el empleado seleccionado, junto con sus comentarios si los tuviese</returns>
        public async Task<List<Vote>> GetByDateRangeForEmployeeVoter(long idEmployee, DateTime dateFrom, DateTime dateTo, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                return await context.Vote
                                .Include(vote => vote.VoteComment)
                                .Include(vote => vote.IdEmployeeVotedNavigation)
                                .Include(vote => vote.IdCompanyAreaNavigation)
                                .ThenInclude(companyArea => companyArea.IdAreaNavigation)
                                .Where(current =>
                                    current.IdEmployeeVoter == idEmployee
                                    && current.Date >= dateFrom
                                    && current.Date < dateTo)
                                .ToListAsync(ct);
            }
        }

        /// <summary>
        /// Función para ir a buscar todos los votos dentro de un rango de fechas y un area de un empleado
        /// </summary>
        /// <param name="idEmployee">Id del empleado votante</param>
        /// <param name="idCompanyArea">Id del area votada</param>
        /// <param name="dateFrom">Fecha desde</param>
        /// <param name="dateTo">Fecha Hasta</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        /// <returns>Voto que emitio el empleado</returns>
        public async Task<Vote> GetByDateRangeAndAreaForEmployeeVoter(long idEmployee, long idCompanyArea, DateTime dateFrom, DateTime dateTo, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                return await context.Vote
                                .Include(vote => vote.VoteComment)
                                .Include(vote => vote.IdEmployeeVotedNavigation)
                                .Include(vote => vote.IdCompanyAreaNavigation)
                                .ThenInclude(companyArea => companyArea.IdAreaNavigation)
                                .FirstOrDefaultAsync(current =>
                                    current.IdEmployeeVoter == idEmployee
                                    && current.IdCompanyArea == idCompanyArea
                                    && current.Date >= dateFrom
                                    && current.Date < dateTo, ct);
            }
        }

        /// <summary>
        /// Función para ir a buscar todos los votos dentro de un rango de fechas para una compañia según areas
        /// </summary>
        /// <param name="idCompany">Id de la compañia</param>
        /// <param name="dateFrom">Fecha desde</param>
        /// <param name="dateTo">Fecha Hasta</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        /// <returns>Ranking de votos según Area dentro del rango de fechas para la compañia</returns>
        public async Task<List<RankingByArea>> GetByDateRangeForCompanyByArea(long idCompany, int month, int year, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                var dateFrom = new DateTime(year, month, 1);
                var dbResult = await context.Vote
                                .Where(current =>
                                    current.IdCompanyAreaNavigation.IdCompany == idCompany
                                    && current.Date >= dateFrom
                                    && current.Date < dateFrom.AddMonths(1))
                                .Select(current => new {
                                    IdEmployee = current.IdEmployeeVoted,
                                    EmployeeDocket = current.IdEmployeeVotedNavigation.Docket,
                                    EmployeeName = current.IdEmployeeVotedNavigation.Name,
                                    current.IdCompanyAreaNavigation.IdArea,
                                    AreaName = current.IdCompanyAreaNavigation.IdAreaNavigation.Name
                                })
                                .ToListAsync<dynamic>(ct);
                return dbResult.GroupBy(current => current.IdArea)
                                .Select(current => new RankingByArea {
                                    IdArea = current.Key,
                                    AreaName = current.Select(currentArea => currentArea.AreaName).Distinct().First(),
                                    EmployeeList = current.Select(currentEmployee => new {
                                            currentEmployee.EmployeeDocket,
                                            currentEmployee.EmployeeName,
                                            currentEmployee.IdEmployee
                                        })
                                        .GroupBy(currentEmployee => currentEmployee.IdEmployee)
                                        .Select(currentEmployee => new EmployeeRanking
                                        {
                                            IdEmployee = currentEmployee.Key,
                                            EmployeeName = currentEmployee.Select(currentEmployee => currentEmployee.EmployeeName).Distinct().First(),
                                            EmployeeDocket = currentEmployee.Select(currentEmployee => currentEmployee.EmployeeDocket).Distinct().First(),
                                            TotalVotes = currentEmployee.Count()
                                        }).OrderByDescending(currentEmployee => currentEmployee.TotalVotes).ToList()
                                }).ToList();
            }
        }

        /// <summary>
        /// Función para ir a buscar todos los votos dentro de un rango de fechas para una compañia según empleados
        /// </summary>
        /// <param name="idCompany">Id de la compañia</param>
        /// <param name="month">Mes para la consulta</param>
        /// <param name="year">Año para la consulta</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        /// <returns>Ranking de votos dentro del rango de fechas de los empleados para la compañia</returns>
        public async Task<List<EmployeeRanking>> GetByDateRangeForCompanyByEmployee(long idCompany, int month, int year, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                var dateRange = new DateTime(year, month, 1);
                var dbResult = await context.Vote
                                .Where(current =>
                                    current.IdCompanyAreaNavigation.IdCompany == idCompany
                                    && current.Date >= dateRange
                                    && current.Date < dateRange.AddMonths(1))
                                .Select(current => new {
                                    IdEmployee = current.IdEmployeeVoted,
                                    EmployeeDocket = current.IdEmployeeVotedNavigation.Docket,
                                    EmployeeName = current.IdEmployeeVotedNavigation.Name,
                                    current.IdCompanyAreaNavigation.IdArea,
                                    AreaName = current.IdCompanyAreaNavigation.IdAreaNavigation.Name
                                })
                                .ToListAsync<dynamic>(ct);
                return dbResult.GroupBy(current => current.IdEmployee)
                                .Select(current => new EmployeeRanking
                                {
                                    IdEmployee = current.Key,
                                    EmployeeName = current.Select(currentEmployee => currentEmployee.EmployeeName).Distinct().First(),
                                    EmployeeDocket = current.Select(currentEmployee => currentEmployee.EmployeeDocket).Distinct().First(),
                                    TotalVotes = current.Count()
                                }).OrderByDescending(current => current.TotalVotes).ToList();
            }
        }

        private Vote Sanitize(Vote toSanitize)
        {
            if (toSanitize.VoteComment != null && !string.IsNullOrEmpty(toSanitize.VoteComment.Comment))
                toSanitize.VoteComment.Comment = toSanitize.VoteComment.Comment.Trim();
            return toSanitize;
        }
    }
}

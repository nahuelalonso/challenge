﻿using Challenge.DataModelMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Challenge.DataManager.Models
{
    /// <summary>
    /// Funciones permitidas contra la base de datos para todo lo que corresponde a la tabla Area
    /// </summary>
    public class AreaModel
    {
        private readonly Func<ChallengeContext> Factory;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="factory">Factory del ChallengeContext</param>
        public AreaModel(Func<ChallengeContext> factory)
        {
            Factory = factory;
        }
        /// <summary>
        /// Función para agregar un area en la base
        /// </summary>
        /// <param name="toAdd">Objeto Area a incorporar en la base</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        /// <returns>Objeto Area incorporado con su Id</returns>
        public async Task<Area> Add(Area toAdd, CancellationToken ct = default)
        {
            var toReturn = await GetByName(toAdd.Name);
            if (toReturn != null) return toReturn;
            using (var context = Factory.Invoke())
            {
                _ = await context.Area.AddAsync(Sanitize(toAdd), ct);
                _ = await context.SaveChangesAsync(ct);
                return toAdd;
            }
        }
        /// <summary>
        /// Función para obtener un area según el nombre, es case insensitive
        /// </summary>
        /// <param name="name">Nombre a buscar</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        /// <returns>Objeto Area correspondiente al nombre buscado</returns>
        public async Task<Area> GetByName(string name, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                return await context.Area.FirstOrDefaultAsync(current => current.Name.ToLower().Trim() == name.ToLower().Trim(), ct);
            }
        }

        private Area Sanitize(Area toSanitize)
        {
            toSanitize.Name = toSanitize.Name.Trim();
            return toSanitize;
        }
    }
}

﻿using Challenge.DataModelMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Challenge.DataManager.Models
{
    /// <summary>
    /// Funciones permitidas contra la base de datos para todo lo que corresponde a la tabla Employee
    /// </summary>
    public class EmployeeModel
    {
        private readonly Func<ChallengeContext> Factory;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="factory">Factory del ChallengeContext</param>
        public EmployeeModel(Func<ChallengeContext> factory)
        {
            Factory = factory;
        }

        /// <summary>
        /// Función para agregar un empleado en la base
        /// </summary>
        /// <param name="toAdd">Objeto Employee a incorporar en la base</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        /// <returns>Objeto Employee incorporado con su Id</returns>
        public async Task<Employee> Add(Employee toAdd, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                _ = await context.Employee.AddAsync(Sanitize(toAdd), ct);
                _ = await context.SaveChangesAsync(ct);
                return toAdd;
            }
        }

        /// <summary>
        /// Función para recuperar una lista de empleados según un nombre de una compañia
        /// </summary>
        /// <param name="idCompany">Id de la compañia en la que trabaja</param>
        /// <param name="name">Nombre a buscar</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        /// <returns>Lista de empleados para la compañia</returns>
        public async Task<List<Employee>> GetByName(long idCompany, string name, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                return await context.Employee.Where(current => current.IdCompany == idCompany && current.Name.Contains(name)).ToListAsync(ct);
            }
        }

        /// <summary>
        /// Función que devuelve un empleado por compañia y legajo
        /// </summary>
        /// <param name="docket">Legajo del empleado</param>
        /// <param name="idCompany">Id de la compañia ne la que trabaja</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        /// <returns>Devuelve un empleado</returns>
        public async Task<Employee> GetByDocketAndIdCompany(string docket, long idCompany, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                return await context.Employee.FirstOrDefaultAsync(current => current.IdCompany == idCompany && current.Docket.ToLower() == docket.ToLower().Trim(), ct);
            }
        }

        /// <summary>
        /// Función para obtener un empleado según su Id en base
        /// </summary>
        /// <param name="id">Id del registro</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        /// <returns>Empleado según Id buscado</returns>
        public async Task<Employee> GetById(long id, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                return await context.Employee.Include(current => current.VoteIdEmployeeVoterNavigation).FirstOrDefaultAsync(current => current.Id == id, ct);
            }
        }

        /// <summary>
        /// Función para buscar todos los empleados de una compañia
        /// </summary>
        /// <param name="idCompany">Id de la compañia en la que trabajan</param>
        /// <param name="ct">Token de cancelación de Asincronismo</param>
        /// <returns>Listado de empleados que trabajan en la compañia</returns>
        public async Task<List<Employee>> GetByIdCompany(long idCompany, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                return await context.Employee.Where(current => current.IdCompany == idCompany).ToListAsync(ct);
            }
        }

        public async Task<EmployeePhoto> GetPhoto(long idEmployee, CancellationToken ct = default)
        {
            using (var context = Factory.Invoke())
            {
                return await context.EmployeePhoto.OrderByDescending(current => current.UploadDate).FirstOrDefaultAsync(current => current.IdEmployee == idEmployee, ct);
            }
        }

        private Employee Sanitize(Employee toSanitize)
        {
            toSanitize.Docket = toSanitize.Docket.Trim();
            toSanitize.Name = toSanitize.Name.Trim();
            return toSanitize;
        }
    }
}

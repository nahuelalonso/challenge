﻿using Challenge.DataManager.Models;
using Challenge.DataModelMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Challenge.DataManager
{
    /// <summary>
    /// Repositorio de la Solución, donde manejamos la obtención, modificación, eliminación y agregado de las entidades en la Base de Datos
    /// </summary>
    public class ChallengeRepository
    {
        private Func<ChallengeContext> Factory { get; }

        /// <summary>
        /// Propiedad que nos permite operar con la compañia que se encuentra en la Base de Datos
        /// </summary>
        public CompanyModel Company { get; }

        /// <summary>
        /// Propiedad que nos permite operar con la empleado que se encuentra en la Base de Datos
        /// </summary>
        public EmployeeModel Employee { get; }

        /// <summary>
        /// Propiedad que nos permite operar con el usuario que se encuentra en la Base de Datos
        /// </summary>
        public UserModel User { get; }

        /// <summary>
        /// Propiedad que nos permite operar con los roles que se encuentra en la Base de Datos
        /// </summary>
        public RoleModel Role { get; }

        /// <summary>
        /// Propiedad que nos permite operar con las areas que se encuentra en la Base de Datos
        /// </summary>
        public AreaModel Area { get; }

        /// <summary>
        /// Propiedad que nos permite operar con los votos que se encuentra en la Base de Datos
        /// </summary>
        public VoteModel Vote { get; }

        /// <summary>
        /// Constructor del Repositorio
        /// </summary>
        /// <param name="factory">Factory del context que vamos a utilizar en la conexión con la base de datos</param>
        public ChallengeRepository (Func<ChallengeContext> factory)
        {
            Factory = factory;
            Company = new CompanyModel(Factory);
            Employee = new EmployeeModel(Factory);
            User = new UserModel(Factory);
            Role = new RoleModel(Factory);
            Area = new AreaModel(Factory);
            Vote = new VoteModel(Factory);
        }
    }
}

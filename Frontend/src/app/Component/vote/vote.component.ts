import { Component, OnInit } from '@angular/core';
import { CompanyService } from 'src/app/Services/company.service';
import { EmployeeService } from 'src/app/Services/employee.service';
import { IdNameResponse } from 'src/app/Services/Response/IdNameResponse';
import { EmployeeResponse } from 'src/app/Services/Response/EmployeeResponse';
import { VoteRequest } from 'src/app/Services/Request/VoteRequest';
import { SnackBarService } from 'src/app/Services/snack-bar.service';
import { LocalStorageService } from 'src/app/Services/local-storage.service';

@Component({
  selector: 'app-vote',
  templateUrl: './vote.component.html',
  styleUrls: ['./vote.component.scss']
})
export class VoteComponent implements OnInit {
  public areas: Array<IdNameResponse> = new Array<IdNameResponse>();
  public employees: Array<EmployeeResponse> = new Array<EmployeeResponse>();
  public votes: Array<any> = new Array<any>();

  constructor(
    private companyService: CompanyService,
    private employeeService: EmployeeService,
    private snackbarService: SnackBarService,
    private storageService: LocalStorageService
  ) {
    this.companyService.getAllAreas().subscribe(res => {
      this.areas = res.response;
      this.filterAreas();
    });
    this.employeeService.getAll().subscribe(res => {
      const data = this.storageService.Get('data');
      res.response.forEach((current: EmployeeResponse) => {
        if ((data.idUser * 1 ) !== current.id) {
          this.employees.push(current);
        }
      });
    });
  }

  private filterAreas() {
    const alreadyVotes = this.storageService.Get('alreadyvote');
    const dateNow = new Date();
    this.areas.forEach (current => {
      let active = true;
      if (alreadyVotes !== undefined && alreadyVotes !== null) {
        const voteDate = new Date(alreadyVotes.date);
        if (voteDate.getMonth() === dateNow.getMonth() && voteDate.getFullYear() === dateNow.getFullYear()) {
          alreadyVotes.votes.forEach(currentVote => {
            if (currentVote.idCompanyArea === current.id) {
              active = false;
            }
          });
        }
      }
      this.votes[current.id] = {
        active,
        companyArea: current.id,
        employee: 0,
        comment: ''
      };
    });
  }

  ngOnInit(): void {
  }

  vote() {
    const toEmit = new Array<VoteRequest>();
    this.votes.forEach(current => {
      if (current.employee === 0) { return; }
      if (!current.active) { return; }
      // tslint:disable-next-line: one-variable-per-declaration
      const toAdd: VoteRequest = {
        idCompanyArea: current.companyArea * 1,
        idEmployee: current.employee * 1,
        comment: current.comment
      };
      toEmit.push(toAdd);
    });
    if (toEmit.length > 0) {
      this.employeeService.vote(toEmit).subscribe(res => {
        if (res.code === 200 || res.code === 206) {
          this.storageService.Set('alreadyvote', {date: new Date(), votes: toEmit});
          this.snackbarService.ShowSuccess(res.response);
          this.filterAreas();
        }
        console.log(res);
      });
    }
  }

}

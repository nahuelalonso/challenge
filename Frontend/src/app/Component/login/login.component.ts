import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { AuthenticateRequest } from 'src/app/Services/Request/AuthenticatonEndpoint';
import { AuthenticationService } from 'src/app/Services/authentication.service';
import { GeneralResponse } from 'src/app/Services/Response/GeneralResponse';
import { HttpErrorResponse } from '@angular/common/http';
import { SnackBarService } from 'src/app/Services/snack-bar.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username = new FormControl('', [Validators.required]);
  password = new FormControl('', [Validators.required, Validators.minLength(6)]);
  hidePassword = true;
  sendLogin = false;

  constructor(
    private authService: AuthenticationService,
    private snackbarService: SnackBarService
    ) {
  }

  getErrorMessage(field: string) {
    switch (field){
      case 'username':
          return  this.username.hasError('required') ? 'Necesitamos que ingreses tu nombre de usuario' :
                  '';
      case 'password':
          return  this.password.hasError('required') ? 'Necesitamos que ingreses tu Contraseña' :
                  this.password.hasError('minlength') ? 'Tu contraseña debe tener al menos 6 caracteres' :
                  '';
    }
  }

  ngOnInit() {
  }

  login() {
    if (this.username.valid && this.password.valid){
      this.sendLogin = true;
      const userToLogin: AuthenticateRequest = {
        username: this.username.value,
        password: this.password.value
      };
      this.authService.login(userToLogin).subscribe(
        (response: GeneralResponse<string>) => {
          this.authService.setUser(response.response);
        },
        (error: HttpErrorResponse) => {
          this.sendLogin = false;
          switch (error.status) {
            case 401:
              this.snackbarService.ShowError('El usuario o la Constraseña son Incorrectos');
              break;
            default:
              this.snackbarService.ShowError('Hubo un error al intentar comunicarnos con el servidor');
              break;
          }
        }
      );
    }
  }
}

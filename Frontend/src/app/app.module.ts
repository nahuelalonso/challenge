import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './Component/login/login.component';
import { LayoutPublicComponent } from './Component/Layout/layout-public/layout-public.component';
import { LayoutPrivateComponent } from './Component/Layout/layout-private/layout-private.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MaterialModule } from './material.module';
import { JwtModule } from '@auth0/angular-jwt';
import { VoteComponent } from './Component/vote/vote.component';
import { NavBarComponent } from './Component/Layout/nav-bar/nav-bar.component';
import { AppKeySecretInterceptor } from './Interceptor/http.interceptor';
export function tokenGetter(): string {
  return JSON.parse(localStorage.getItem('access_token'));
}
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LayoutPublicComponent,
    LayoutPrivateComponent,
    VoteComponent,
    NavBarComponent
  ],
  imports: [
    NgbModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MaterialModule,
    NgbModule,
    JwtModule.forRoot({
      config: {
        tokenGetter,
        whitelistedDomains: [ 'localhost:44352' ]
      }
    })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AppKeySecretInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export class RankingResponse<TypeRanking> {
    date: Date;
    data: TypeRanking;
}
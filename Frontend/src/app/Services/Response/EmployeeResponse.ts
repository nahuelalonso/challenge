import { UserResponse } from './UserResponse';

export class EmployeeResponse {
    id: number;
    docket: string;
    name: string;
    user: UserResponse;
}
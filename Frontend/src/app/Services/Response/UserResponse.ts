export class UserResponse {
    id: number;
    username: string;
    roleName: string;
}
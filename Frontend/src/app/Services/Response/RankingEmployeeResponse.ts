export class RankingEmployeeResponse {
    idEmployee: number;
    employeeDocket: string;
    employeeName: string;
    totalVotes: number;
}
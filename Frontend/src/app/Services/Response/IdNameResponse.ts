export class IdNameResponse {
    id: number;
    name: string;
}
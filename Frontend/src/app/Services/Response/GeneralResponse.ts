export class GeneralResponse<TypeResponse> {
    code: number;
    status: string;
    errors: Array<ErrorResponse>;
    response: TypeResponse;
}

export class ErrorResponse {
    code: string;
    reason: string;
}
import { IdNameResponse } from './IdNameResponse';
import { EmployeeResponse } from './EmployeeResponse';

export class CompanyResponse {
    id: number;
    name: string;
    areas: Array<IdNameResponse>;
    employee: EmployeeResponse;
}
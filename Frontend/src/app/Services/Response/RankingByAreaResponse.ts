import { RankingEmployeeResponse } from './RankingEmployeeResponse';

export class RankingByAreaResponse {
    idArea: number;
    areaName: string;
    employees: Array<RankingEmployeeResponse>;
}
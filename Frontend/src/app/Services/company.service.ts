import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { GeneralResponse } from './Response/GeneralResponse';
import { IdNameResponse } from './Response/IdNameResponse';
import { CompanyRequest } from './Request/CompanyRequest';
import { CompanyResponse } from './Response/CompanyResponse';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(
    private httpClient: HttpClient
    ) { }

  create(company: CompanyRequest) {
    const endpoint = `${environment.endpoint.urlBase}${environment.endpoint.resources.company}`;
    return this.httpClient
                    .post<GeneralResponse<CompanyResponse>>(endpoint, company);
  }

  getAllAreas() {
    const endpoint = `${environment.endpoint.urlBase}${environment.endpoint.resources.company}areas`;
    return this.httpClient
                    .get<GeneralResponse<Array<IdNameResponse>>>(endpoint);
  }
}

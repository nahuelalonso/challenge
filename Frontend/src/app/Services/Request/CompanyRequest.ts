import { EmployeeRequest } from './EmployeeRequest';

export class CompanyRequest {
    name: string;
    areas: Array<string>;
    employee: EmployeeRequest;
}
import { UserRequest } from './UserRequest';

export class EmployeeRequest {
    docket: string;
    namr: string;
    user: UserRequest;
}
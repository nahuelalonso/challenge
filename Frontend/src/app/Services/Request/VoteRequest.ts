export class VoteRequest {
    idEmployee: number;
    idCompanyArea: number;
    comment: string;
}
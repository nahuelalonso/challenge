import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { GeneralResponse } from './Response/GeneralResponse';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(
    private httpClient: HttpClient
    ) { }

  getTotalEmployees() {
    const endpoint = `${environment.endpoint.urlBase}${environment.endpoint.resources.report}totalEmployees`;
    return this.httpClient
                    .get<GeneralResponse<number>>(endpoint);
  }

  getRankingByMonth(dateFrom: Date, dateTo: Date) {
    const monthFrom = dateFrom.getMonth() + 1;
    const yearFrom = dateFrom.getFullYear();
    const monthTo = dateTo.getMonth() + 1;
    const yearTo = dateTo.getFullYear();
    // tslint:disable-next-line: max-line-length
    const endpoint = `${environment.endpoint.urlBase}${environment.endpoint.resources.report}rankingByMonth/from/${monthFrom}-${yearFrom}/to/${monthTo}-${yearTo}`;
    return this.httpClient
                    .get<GeneralResponse<number>>(endpoint);
  }

  getRankingByArea(dateFrom: Date, dateTo: Date) {
    const monthFrom = dateFrom.getMonth() + 1;
    const yearFrom = dateFrom.getFullYear();
    const monthTo = dateTo.getMonth() + 1;
    const yearTo = dateTo.getFullYear();
    // tslint:disable-next-line: max-line-length
    const endpoint = `${environment.endpoint.urlBase}${environment.endpoint.resources.report}rankingByArea/from/${monthFrom}-${yearFrom}/to/${monthTo}-${yearTo}`;
    return this.httpClient
                    .get<GeneralResponse<number>>(endpoint);
  }
}

import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class SnackBarService {

  constructor ( private snackBar: MatSnackBar ) { }

  ShowError ( message: string, action?: string ) {
    return this.snackBar.open(message, action || "Cerrar", {
      duration: 300000,
      panelClass: 'error'
    })
  }

  ShowSuccess ( message: string, action?: string ) {
    return this.snackBar.open(message, action || "Cerrar", {
      duration: 6000,
      panelClass: 'success'
    })
  }
}

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  Get(key: string): any {
    return JSON.parse(window.localStorage.getItem(key));
  }

  Clear() {
    window.localStorage.clear();
  }

  // tslint:disable-next-line: ban-types
  Set(key: string, value: Object) {
    window.localStorage.setItem(key, JSON.stringify(value));
  }

  Remove(key: string) {
    window.localStorage.removeItem(key);
  }
}

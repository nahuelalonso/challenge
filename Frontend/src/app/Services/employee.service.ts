import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { GeneralResponse } from './Response/GeneralResponse';
import { EmployeeResponse } from './Response/EmployeeResponse';
import { EmployeeRequest } from './Request/EmployeeRequest';
import { VoteRequest } from './Request/VoteRequest';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(
    private httpClient: HttpClient
    ) { }

  getAll() {
    const endpoint = `${environment.endpoint.urlBase}${environment.endpoint.resources.employee}`;
    return this.httpClient
                    .get<GeneralResponse<Array<EmployeeResponse>>>(endpoint);
  }

  create(employee: EmployeeRequest) {
    const endpoint = `${environment.endpoint.urlBase}${environment.endpoint.resources.employee}`;
    return this.httpClient
                    .post<GeneralResponse<Array<EmployeeResponse>>>(endpoint, employee);
  }

  vote(employee: Array<VoteRequest>) {
    const endpoint = `${environment.endpoint.urlBase}${environment.endpoint.resources.employee}vote`;
    return this.httpClient
                    .post<GeneralResponse<string>>(endpoint, employee);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { LocalStorageService } from './local-storage.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AuthenticateRequest } from './Request/AuthenticatonEndpoint';
import { GeneralResponse } from './Response/GeneralResponse';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private storageService: LocalStorageService,
    private jwtHelper: JwtHelperService
    ) { }

  public login(user: AuthenticateRequest): Observable<GeneralResponse<string>> {
    const endpoint = `${environment.endpoint.urlBase}${environment.endpoint.resources.authentication}login`;
    return this.httpClient
                    .post<GeneralResponse<string>>(endpoint, user)
                    .pipe<GeneralResponse<string>>(
                      catchError(this.handleError)
                    );
  }

  private handleError(error: HttpErrorResponse) {
    if ( error.error instanceof ErrorEvent ) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(error);
  }

  public isLogin(): boolean {
    try {
      if (this.jwtHelper.isTokenExpired(this.storageService.Get('access_token').toString())) {
        throw new Error('Token Expired');
      }
    } catch {
      this.logout();
      return false;
    }
    return true;
  }

  public logout(): void {
    this.storageService.Clear();
    this.router.navigate(['login']);
  }

  public setUser(token: string): void {
    const data = this.jwtHelper.decodeToken(token);
    this.storageService.Set('data', data);
    this.storageService.Set('access_token', token);
    this.router.navigate(['vote']);
  }
}

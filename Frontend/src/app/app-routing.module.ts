import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './Component/login/login.component';
import { LayoutPublicComponent } from './Component/Layout/layout-public/layout-public.component';
import { LayoutPrivateComponent } from './Component/Layout/layout-private/layout-private.component';
import { VoteComponent } from './Component/vote/vote.component';


const routes: Routes = [
  {
    path: 'login',
    component: LayoutPublicComponent,
    children: [
      {
        path: '',
        component: LoginComponent
      }
    ]
  },
  {
    path: 'vote',
    component: LayoutPrivateComponent,
    children: [
      {
        path: '',
        component: VoteComponent
      }
    ]
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

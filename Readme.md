# Challenge #

El reto consta en la realización de una solución de backend que exponga una **API REST Segura**.

## Requerimiento ##

Se debe realizar una solución en la cuál un empleado tiene que emitir un voto hacia una persona en un área específica de la empresa en donde son compañeros de trabajo

### Restricciones ###

* El empleado no puede votarse a sí mismo
* Los votos son sobre una persona en distintas áreas
* Un empleado posee sólo un voto por área
* Los votos pueden contener un comentario en forma opcional
* Los votos caducan al mes y no son acumulables
* Deberán existir dos *Roles*
	* Employee, tendrá acceso limitado (emitir voto, completar su foto de perfil)
	* Admin, tendrá acceso a todo el sistema
* Admin podrá:
	* Obtener reporte de:
		* Empleados más votados por mes
		* Cantidad de empleados registrados
		* Empleados más votados por área
* Uso de *Framework .Net Core*
* Uso de un ORM tipo *nHibernate o EntityFramework*
* Motor de Base de Datos a elección
* Uso de *Git* como Gestor de versiones
* Documentar código al estilo *JavaDocs*
* Documentar endpoints al estilo *Swagger o Postman*

### Entorno seleccionado ###

* .Net Core 3.1
* EntityFramework
* PostgreSQL 10
* Documentación de Endpoint de Postman
* ToadModeler para el modelado de la base de datos

### Justificación ###

* [Documentación de Endpoint de Postman](https://web.postman.co/collections/832873-62a57e18-6c98-4518-8712-21d27f2bcdbd?version=latest&workspace=f5c664fe-d88b-4110-be26-91af06a65ca1)
* [Colección de Prueba y testeo de Postman](https://www.getpostman.com/collections/1c955173627105900b21)
* [Url del repositorio](https://bitbucket.org/nahuelalonso/challenge/src)

Para realizar la solución se escogió una arquitectura de n-capas donde cada una de las capas sólo le da servicios a la capa superior y toma servicios de la inferior, pero también tiene una capa distribuida que es la capa Helper, la cual incorpora código que puede ayudar a realizar ciertas tareas, pero que no tiene que ver con la lógica de negocio de la solución. Al momento del diseño del sistema se opto por realizar el diseño de la base de datos primero, por lo que se requirió realizar un scaffolding del DBContext que requiere EntityFramework, para que realice el mapeo de la base en el código.

La primera capa que es la que esta publica a la web es la capa ___Endpoints___, esta capa es la encargada de realizar las correspondientes validaciones de lo que viene del exterior. En ella vamos a ver que manejamos el tema de los roles y autenticación del usuario al momento de realizar el request mediante un role específico. A su vez también tiene la lógica para validar todos los request como así los parámetros que se envían por la ruta a la que se intenta acceder. Esta capa consume los servicios de la capa ___BusinessLayer___.

La capa ___BusinessLayer___ tiene toda la lógica de negocio que requiere la solución, en ella se validan que los votos sean únicos como así también la existencia de ciertas entidades en la base de datos antes del intento de agregar una nueva. Esta capa consume los servicios de la capa ___Helper___ quien contiene una ayudante para poder encriptar y desencriptar la password del usuario, y también consume los servicios de la capa ___DataManager___.

La capa ___DataManager___ contiene los repositorios necesarios para poder realizar las operaciones contra la base de datos que sean necesarias, esta capa consume el mapeo que se genero mediante el anterior mencionado scaffolding, que se encuentra en otra capa denominada ___DataModelMapper___, quien contiene el mapeo de la base de datos que se diseño con anterioridad.

En cuanto a la autenticación, se decidió realizarla mediante JWT, ya que tenemos la posibilidad de enviar al cliente el token con una clave publica, para que el cliente pueda validar el JWT, y del lado del servidor manejamos un clave privada para poder verificar en cada request que el token sea válido.